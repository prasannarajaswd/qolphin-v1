<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Branch extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'cin', 'branch_name', 'short_name', 'addresss_1', 'addresss_2', 'city_id', 'state_id', 'country_id', 'latitude', 'longitude', 'is_active', 'city_name', 'country_name', 'state_name', 'pincode'
    ];

    
}

