<?php

namespace  App\Common;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use Tzsk\Sms\Facade\Sms;
use App\Notifications;


Class Commonuse extends Controller{

    public function auto_notification(){


        $headers = array(
            'Authorization: token 3c0b6a125baa5d227150cd44ee8c5e891b1e53d7'
        );

        $durl = "http://18.223.172.188/api/push/notify";

        // {
        //     "channel" :"qol-a06d9569cf6c9070",
        //     "message" :"Happy Diwali",
        //     "title":"Wishes",
        //     "platform":"android"
        // }

        $employees = DB::table('employees as emp')
        ->where('emp.is_active', 1)
        ->join('shifts as sh', 'emp.shift_id', 'sh.id')
        ->join('user_channels as uc', 'emp.user_id', 'uc.user_id')
        ->where('uc.is_active', 1)
        ->select('emp.*', 'sh.start_time', 'sh.end_time', 'uc.channel', 'uc.platform')
        ->get();

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt ($curl, CURLOPT_URL, $durl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);  
        curl_setopt($curl, CURLOPT_POST, TRUE);

        foreach ($employees as $employee) {
            $start_time = $employee->start_time;
            $end_time = $employee->end_time;
            
            $notifications['user_id'] = $user_id = $employee->user_id;
            $notifications['title'] = $title = 'Reminder: PunchIn/PunchOut';
            $notifications['created_date'] = date('Y-m-d H:i:s');
            $notifications['des'] = $des = 'Dont forget to Punchin/Punchout';
            $notifications['is_redirectable'] = 0;

            Notifications::create($notifications);

            $channel = $employee->channel;
            $platform = $employee->platform;

            $inputs = array('channel'=>$channel, 'message'=>$des, 'title'=>$title, 'platform'=>$platform);
            
            curl_setopt($curl, CURLOPT_POSTFIELDS, $inputs);
            $result = curl_exec ($curl);

        }

        curl_close ($curl);

        return true;
    }

    //send otp
    public function send_sms($message, $number){

        if($message != "" && $number != ""){
            $number = "+91".$number;
            $result = Sms::send($message, function($sms) use ($number) {
                $sms->to([$number]); # The numbers to send to.
            });
        } 

        return true;

    }

    //add users
    public function add_users($input){

        $validator = Validator::make($input, [ 
            'name' => 'required', 
            'email' => 'required', 
            'mobile' => 'required',
            'password' => 'required', 
            'user_type' => 'required'
        ]);

        if ($validator->fails()) { 
            $success['status'] = 400;           
        }else{
            $user = User::create($input);
            $success['status'] = 200; 
            $success['user_id'] =  $user->id;
        }

        return $success;

    }

     //update users
    public function update_users($input, $id){

        $validator = Validator::make($input, [ 
            'name' => 'required', 
            'email' => 'required', 
            'mobile' => 'required'
        ]);

        if ($validator->fails()) { 
            $success['status'] = 400;           
        }else{

            $user_update = User::findOrFail($id);
            $user_update->update($input);
            $success['status'] = 200; 
            $success['user_id'] =  $id;
        }

        return $success;

    }

    //check branch previleges
    public function branch_previleges($branch_id, $during_process="", $answer=""){

        $branch_prvileges = DB::table('branch_previleges as bp')
        ->join('previleges as p', 'bp.previlege_id', 'p.id')
        ->where('branch_id', $branch_id);
        
        if($during_process == "CHECKIN"){
            $branch_prvileges = $branch_prvileges->where('p.during_process', 'checkin');
        }
        if($during_process == "CHECKOUT"){
            $branch_prvileges = $branch_prvileges->where('p.during_process', 'checkout');
        }
        if($answer == 1){
            $branch_prvileges = $branch_prvileges->where('bp.answer', 1);
        }

        $branch_prvileges = $branch_prvileges->select('p.previlege_type', 'p.previlege', 'bp.answer')->get();
        
        $success['status'] = 0; 
        if(count($branch_prvileges) > 0){
            $success['status'] = 200; 
            $success['branch_id'] =  $branch_id;
            $result =  [];
            foreach($branch_prvileges as $branch_prvilege){
                $previlege_type = $branch_prvilege->previlege_type;
                if(!isset($result[$previlege_type])){
                    $result[$previlege_type] =  $branch_prvilege->answer;
                }
            }
            
            $success['previleges'] = $result;
        }

        return $success;
        
    }

    //upload image
    public function upload_image(Request $request){

    }

    //get current time
    public function current_time(){
        date_default_timezone_set('Asia/Kolkata');
        return date("H:i:s");
    }

    //get current date
    public function current_date(){
        date_default_timezone_set('Asia/Kolkata');
        return date("Y-m-d");
    }

    public function time_ch($time){
        return date('H:i A', strtotime($time));
    }

    

    //compare face faceX
    public function compare_face_facex($img1, $img2){
        $user_id = '20acac97c9e059cc76f4';
        $user_key = '8d4b89e37b0e8dbdd28e';

        $durl = "http://www.facexapi.com/compare_faces?face_det=1";

        $inputs = array('img_1'=>$img1, 'img_2'=>$img2);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, Array("user_id: ".$user_id, "user_key: ".$user_key));
        curl_setopt ($curl, CURLOPT_URL, $durl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);	
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $inputs);
        $result = curl_exec ($curl);
        curl_close ($curl);
        
        return json_decode($result);

    }


    public function check_auth(){
        if(Auth::check()){

        }else{
            return response()->json(['error'=>'unauthorised'], 400); 
        }
    }

}