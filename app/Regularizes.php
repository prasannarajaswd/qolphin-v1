<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Regularizes extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'attendance_date', 'checkin_time', 'checkout_time', 'reason', 'reason_id', 'approved_by', 'approved_date', 'approval_status', 'attendance_id'
    ];

    
}

