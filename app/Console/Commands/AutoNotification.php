<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Common\Commonuse;


class AutoNotification extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically send notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->common = new Commonuse();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->common->auto_notification();
    }
}
