<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\AutoNotification::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('auto:notification')->everyFiveMinutes();//Asia/Calcutta'
        $schedule->command('auto:notification')->timezone('Asia/Calcutta')->dailyAt('08:55');
        $schedule->command('auto:notification')->timezone('Asia/Calcutta')->dailyAt('09:00');
        $schedule->command('auto:notification')->timezone('Asia/Calcutta')->dailyAt('09:10');
        $schedule->command('auto:notification')->timezone('Asia/Calcutta')->dailyAt('18:25');
        $schedule->command('auto:notification')->timezone('Asia/Calcutta')->dailyAt('18:30');
        $schedule->command('auto:notification')->timezone('Asia/Calcutta')->dailyAt('18:35');
        $schedule->command('auto:notification')->timezone('Asia/Calcutta')->dailyAt('10:55');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
