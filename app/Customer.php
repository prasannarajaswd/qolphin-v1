<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Customer extends Authenticatable
{
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_name', 'user_id', 'email', 'mobile', 'alt_mobile', 'addresss_1', 'addresss_2', 'city_id', 'state_id', 'country_id', 'company_proof', 'is_verify_email', 'is_verify_mobile', 'is_active',
    ];

    
}

