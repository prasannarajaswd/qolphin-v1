<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use Mail;
use App\Token;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use Session;
use Redirect;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    public function showLoginForm()
    {
        return view('v1.login');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(){

        
        Session::flush();
        Auth::logout();
        return Redirect::to("/");
        
    }

    public function login(Request $request)  {   

        $result = $request->all();

        $res = $result['values'];

        $result = User::where('email', $res['email'])->where('is_active', 1)
        ->where('user_type', 'C')->first();
        
        if($result){

            if (Hash::check($res['password'], $result->password))
            {

                Auth::login($result);

                $res = 'Success';
                $code = 200;

                return response()->json(['response' => $res, 'code'=>$code], 200);

            }else{
                return response()->json(['response' => 'Please check email/password', 'code'=>400], 200);
            }
        }else{
            return response()->json(['response' => 'Your email is not register', 'code'=>400], 200);
        }  
        
        
    }
}
