<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Customer; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;


class CustomerController extends Controller 
{
    public $successStatus = 200;

    public function __construct()
    {
        $this->common = new Commonuse();
    }


    /** 
     * Customer add api
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function customer_add(Request $request){

        $req= $request->all();

        //$values = $req['values'];

        $validator = Validator::make($req, [ 
            'customer_name' => 'required',
            'email' => 'required|email|unique:customers',
            'mobile' => 'required|unique:customers',
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }

        $input = $req; 
        
        $user_type = "C";

        //user create

        $user['name'] = $input['customer_name']; 
        $user['email'] = $input['email']; 
        $user['mobile'] = $input['mobile']; 
        $user['password'] = bcrypt("Drinjal@123"); 
        $user['user_type'] = $user_type; 
         
        $res = $this->common->add_users($user);
        if($res['status'] == 200){

            $input['user_id'] = $res['user_id'];

            $customer = Customer::create($input);

            $result['customer_id'] = $customer->id;

            $success = $result;

            return response()->json(['response' => "created successfully", 'success' => $success], $this->successStatus); 
            
        }else{
            return response()->json(['error'=>"User not created"], 400); 
        }
        

    }


    /** 
     * Customer register api
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function customer_register(Request $request){

        $validator = Validator::make($request->all(), [ 
            'customer_name' => 'required',
            'email' => 'required|email|unique:customers',
            'mobile' => 'required|unique:customers',
            'addresss_1' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'country_id' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }

        $input = $request->all(); 
        
        $user_type = "C";

        //user create

        $user['name'] = $input['customer_name']; 
        $user['email'] = $input['email']; 
        $user['mobile'] = $input['mobile']; 
        $user['password'] = bcrypt("Date@123"); 
        $user['user_type'] = $user_type; 
         
        $res = $this->common->add_users($user);
        if($res['status'] == 200){

            $input['user_id'] = $res['user_id'];

            $customer = Customer::create($input);

            $result['customer_id'] = $customer->id;

            $success = $result;

            return response()->json(['success' => $success], $this->successStatus); 
            
        }else{
            return response()->json(['error'=>"User not created"], 400); 
        }
        

    }
    
    
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(Request $request){
        
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required',
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }
        
        $input = $request->all(); 
        $mobile = $input['mobile']; 
        
        $get_users = DB::table('users')->where('mobile', $mobile)->first();

        if($get_users){

            if($get_users->is_verify_otp == "0"){

                //send otp
                $user_id = $get_users->id;
                $otp = '1234';
                $result['otp'] = $otp;
                $result['mobile'] = $mobile;
                $result['new_register'] = 'yes';
                $result['next_screen'] = 'otp';
                $result['user_id'] = $user_id;

                DB::table('users')->where('id', $user_id)->update(['otp'=>$otp]);

            }else{

                $result['new_reg'] = 'no';
                

                //qpin_enabled check
                $is_qpin_enabled = 1;
                if($is_qpin_enabled == 1){
                    $result['next_screen'] = 'qpin';
                }else{
                    $result['next_screen'] = 'home';
                }

                $result['user'] = $get_users;

            }

            $success = $result;

            return response()->json(['success' => $success], $this->successStatus); 

        }else{
            return response()->json(['error'=>"mobile number not registered"], 400); 
        }

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            //$success['token'] =  $user->createToken('MyApp')->accessToken; 
            $success['user'] =  $user; 
            return response()->json(['success' => $success], $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    

}
