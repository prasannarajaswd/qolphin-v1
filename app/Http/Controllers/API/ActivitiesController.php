<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Customer;
use App\Branch; 
use App\Employee;
use App\Dashboard; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;


class ActivitiesController extends Controller 
{
    public $successStatus = 200;

    public function __construct()
    {
        $this->common = new Commonuse();
    }


    /** 
     * activity menu get
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function activity_list(Request $request){

        $values['ifone_1'] = 'visible';
        $values['ifzero_0'] = 'invisible';
        $values['iftwo_2'] = 'disabled';

        $input['customer_id'] = Auth::user()->customer_id;
        $input['branch_id'] = Auth::user()->branch_id;
        $user_id = Auth::user()->id;

        $is_rh = DB::table('employees')->where('user_id', $user_id)->where('is_reporting_head', 1)->count();

        $result['my_approvals'] = 0;
        if($is_rh > 0){
            $result['my_approvals'] = 1;
        }

       
        //card 1
        $card['title'] = 'Attendence';
        $card['route'] = 'Attendence';
        $card['subtitle'] = 'View your Attendence';
        $card['icon'] = 'instagram';
        $card['iconColor'] = '#d04119';
        $card['bgColor'] = '#fff0eb';
        $card['textColor'] = '#dd978b';
        $card['visiblity'] = 1;
        
        $notes = [ ['title'=>'PR', 'count'=>10], ['title'=>'AB', 'count'=>10] ];

        $card['notes'] = $notes;

        $cards[] = $card; 
        //end card

        //card
        $card['title'] = 'Leave';
        $card['route'] = 'Leave';
        $card['subtitle'] = 'Apply or View History';
        $card['icon'] = 'dribbble';
        $card['iconColor'] = '#36a640';
        $card['bgColor'] = '#edfbee';
        $card['textColor'] = '#95c59d';
        $card['visiblity'] = 2;

        $notes = [ ['title'=>'PE', 'count'=>0], ['title'=>'AP', 'count'=>0] ];

        $card['notes'] = $notes;

        $cards[] = $card;
        //end

        //card
        $card['title'] = 'Comp Off';
        $card['route'] = 'Comp Off';
        $card['subtitle'] = 'View your compoff';
        $card['icon'] = 'aliwangwang-o1';
        $card['iconColor'] = '#9c0ed2';
        $card['bgColor'] = '#faedfe';
        $card['textColor'] = '#b888c2';
        $card['visiblity'] = 2;

        $notes = [ ['title'=>'PE', 'count'=>0], ['title'=>'AP', 'count'=>0] ];

        $card['notes'] = $notes;

        $cards[] = $card;
        //end
        
        $result['activities'] = $cards;

        $result['status_boolean_values'] = $values;

        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

    }


    /** 
     * activity menu get
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function approval_list(Request $request){

        $values['ifone_1'] = 'visible';
        $values['ifzero_0'] = 'invisible';
        $values['iftwo_2'] = 'disabled';

        $input['customer_id'] = Auth::user()->customer_id;
        $input['branch_id'] = Auth::user()->branch_id;
        $user_id = Auth::user()->id;


        $card['title'] = 'Attendence';
        $card['route'] = 'AttendenceApproval';
        $card['subtitle'] = 'Approve daily Attendence';
        $card['icon'] = 'instagram';
        $card['iconColor'] = '#d04119';
        $card['bgColor'] = '#fff0eb';
        $card['textColor'] = '#dd978b';
        $card['visiblity'] = 1;

        $notes = [ ['title'=>'PE', 'count'=>0], ['title'=>'AP', 'count'=>0] ];

        $card['notes'] = $notes;

        $cards[] = $card;

        $card['title'] = 'Profile';
        $card['route'] = 'ProfileApproval';
        $card['subtitle'] = 'Approve uploaded DP';
        $card['icon'] = 'rocket1';
        $card['iconColor'] = '#a3b7e9';
        $card['bgColor'] = '#ecf2fe';
        $card['textColor'] = '#cfdbff';
        $card['visiblity'] = 1;

        $notes = [ ['title'=>'PE', 'count'=>0], ['title'=>'AP', 'count'=>0] ];

        $card['notes'] = $notes;

        $cards[] = $card;

        $card['title'] = 'Regularize';
        $card['route'] = 'RegularizeApproval';
        $card['subtitle'] = 'Approve Regularizations';
        $card['icon'] = 'rocket1';
        $card['iconColor'] = '#a3b7e9';
        $card['bgColor'] = '#ecf2fe';
        $card['textColor'] = '#cfdbff';
        $card['visiblity'] = 1;

        $notes = [ ['title'=>'PE', 'count'=>0], ['title'=>'AP', 'count'=>0] ];

        $card['notes'] = $notes;

        $cards[] = $card;


        // $card['title'] = 'Leave';
        // $card['route'] = 'Leave';
        // $card['subtitle'] = 'Approve leaves';
        // $card['icon'] = 'dribbble';
        // $card['iconColor'] = '#36a640';
        // $card['bgColor'] = '#edfbee';
        // $card['textColor'] = '#95c59d';
        // $card['visiblity'] = 2;

        // $notes = [ ['title'=>'PE', 'count'=>0], ['title'=>'AP', 'count'=>0] ];

        // $card['notes'] = $notes;

        // $cards[] = $card;

        // $card['title'] = 'Comp Off';
        // $card['route'] = 'Comp Off';
        // $card['subtitle'] = 'Approve Comp offs';
        // $card['icon'] = 'aliwangwang-o1';
        // $card['iconColor'] = '#9c0ed2';
        // $card['bgColor'] = '#faedfe';
        // $card['textColor'] = '#b888c2';
        // $card['visiblity'] = 2;

        // $notes = [ ['title'=>'PE', 'count'=>0], ['title'=>'AP', 'count'=>0] ];

        // $card['notes'] = $notes;

        // $cards[] = $card;

        

        $result['activities'] = $cards;        

        $result['status_boolean_values'] = $values;

        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

    }
    
    
    

    

}
