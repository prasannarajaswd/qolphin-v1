<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Customer;
use App\Branch; 
use App\Employee;
use App\Dashboard; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;


class DashboardController extends Controller 
{
    public $successStatus = 200;

    public function __construct()
    {
        $this->common = new Commonuse();
    }


    /** 
     * Employee add api
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function dashboard(Request $request){

        if (Auth::user()){
            $user = Auth::user();

            $auths['header_1'] = 'Authorization';
            $auths['header_1_value'] = 'Bearer'.' '.$user->createToken('MyApp')->accessToken;
            $auths['header_2'] = 'Accept';
            $auths['header_2_value'] = 'application/json';
            
            $result['headers'] =  $auths; 
        }else{
            return response()->json(['error'=>'Unauthorised', 'code'=>401], 401);
        }
        
        $input = $request->all();

        $input['customer_id'] = Auth::user()->customer_id;
        $input['branch_id'] = Auth::user()->branch_id;
        $user_id = Auth::user()->id;

        //notificatioins
        $notifications_ct = DB::table('notifications')->where('user_id', $user_id)->where('is_active', 1)->count();
        if($notifications_ct > 0){
            $result['is_notification'] = 1;
        }else{
            $result['is_notification'] = 0;
        }

        $get_employees = DB::table('employees')->where('user_id', $user_id)->first();

        $input['employee_id'] = $employee_id = $get_employees->id;

        $result['employee'] = $input;

        $customer_id = $input['customer_id'];
        $branch_id = $input['branch_id'];
        $get_branches = DB::table('branches')->where('id', $branch_id)->first();
        $result['branch'] = $get_branches;

        //get previleges
        $res = $this->common->branch_previleges($branch_id);
        
        if($res['status'] == 0){
            return response()->json(['error'=>'Please set branch previleges', 'code'=>400], 400);  
        }
        
        $result['previleges'] = $res['previleges'];

        $employee_id = $input['employee_id'];

        $get_employees = DB::table('employees')->where('id', $employee_id)->first();
        $profile_pic = $get_employees->profile_pic;

        if($profile_pic == "" || $profile_pic == null){
            //defaulter image
            $default_img = "https://qolphin-assets.s3.us-east-2.amazonaws.com/default_image.png";

            $profile_img = $default_img;

        }else{

            $profile_img = $profile_pic;
        }

        $is_set_profile_pic = $get_employees->profile_pic_status;

        $result['profile_picture'] = $profile_img;
        $result['is_set_profile_pic'] = (int)$is_set_profile_pic;
        $result['full_name'] = $get_employees->first_name." ".$get_employees->last_name;

        //shift enabled
        $result['is_shift_enabled'] = 1;

        //shifts
        $get_shifts = DB::table('shifts')->where('id', $get_employees->shift_id)->first();
        $start_time=$get_shifts->start_time;
        $end_time=$get_shifts->end_time;

        $shifts["start_time"] = date('H:i A', strtotime($start_time));
        $shifts["end_time"] = date('H:i A', strtotime($end_time));
        $shifts["scheduled"] = "On time";
        $result['shifts'] = $shifts;


        $current_date = date('Y-m-d');

        $attendances = DB::table('attendances')->where('employee_id', $employee_id)->where('attendance_date', $current_date)->first();

        //checkin and checkout logs
        $logs['is_checkin_time'] = 0;
        $logs['is_checkout_time'] = 0;
        $logs['checkin_time'] = '';
        $logs['checkout_time'] = '';

        //checkin / checkout button enabled
        $result['checkin_enabled'] = 0;
        $result['checkout_enabled'] = 0;

        if($attendances){

            $result['checkin_enabled'] = $attendances->is_checkin;
            $result['checkout_enabled'] = $attendances->is_checkout;

            if($attendances->checkin_time){
                $logs['is_checkin_time'] = 1;
                $result['checkin_enabled'] = 1;
            }
            if($attendances->checkout_time){
                $logs['is_checkout_time'] = 1;
                $result['checkout_enabled'] = 1;
            }

            if($attendances->checkin_time != null && $attendances->checkin_time != ""){
                $logs['checkin_time'] = date('H:i A', strtotime($attendances->checkin_time));
            }

            if($attendances->checkout_time != null && $attendances->checkout_time != ""){
                $logs['checkout_time'] = date('H:i A', strtotime($attendances->checkout_time));
            }
                        
        }

        $result['logs'] = $logs;

        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

    }
    
    
    

    

}
