<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Customer;
use App\Branch; 
use App\Employee;
use App\Dashboard; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;
use Log;
use Storage;
use Tzsk\Sms\Facade\Sms;



class CommonController extends Controller 
{
    public $successStatus = 200;

    public function __construct()
    {
        $this->common = new Commonuse();
    }


    //send otp
    public function send_sms(Request $request){

        $message = "Test";
        $number = "+919442640132";

        if($message != "" && $number != ""){
            $result = Sms::send($message, function($sms) use ($number) {
                $sms->to([$number]); # The numbers to send to.
            });
        } 

    }
    
    /** 
     * Employee add api
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function upload_file(Request $request){

        $requestData = $request->all();

        //print_r($requestData);exit;

        if(isset($requestData['data'])){

            $file = 'data:image/png;base64,'.$requestData['data'];

            $filename = rand(1000000, 10000000).'.png';

            $t = Storage::disk('s3')->put($filename, file_get_contents($file), 'public');

            $filename = Storage::disk('s3')->url($filename);
                          
            $result['url'] = $filename;

            $success = $result;

            return response()->json(['success' => $success, 'code'=>200], 200);

        }else{
            return response()->json(['error'=>"File is required", 'code'=>400], 400);
        }   
        
        // if ($request->hasFile('file')) {
            
        //     $filename = time().'.'.$request->file->getClientOriginalExtension();

        //     $t = Storage::disk('s3')->put($filename, file_get_contents($request->file), 'public');

        //     $filename = Storage::disk('s3')->url($filename);
                          
        //     $result['url'] = $filename;

        //     $success = $result;

        //     return response()->json(['success' => $success, 'code'=>200], 200);

        // }else{

        //     return response()->json(['error'=>"File is required", 'code'=>400], 400);
        // }

    }

    //function to construct search query
    //input:NA
    //output: return true/false
    // public function upload_file($params, $base64 = "", $file="", $file_url="") {

    //     if (empty($base64)) {

    //         if (!empty($file_url)) {
    //             // echo "string1";
    //             // exit();
    //             $filename = time().'.'.$file_url->getClientOriginalExtension();
    //         }
    //         else{

    //             $filename = time().'.'.$params->branch_image->getClientOriginalExtension();
    //         }
    //         // dd($filename);
    //         $image = $params->file($file);

    //         $t = Storage::disk('s3')->put($filename, file_get_contents($image), 'public');

    //     }
    //     else{

    //         $filename = $file."_".time().".png";

    //         $t = Storage::disk('s3')->put($filename, file_get_contents($params->cropped_image_data), 'public');
    //     }

    //     $filename = Storage::disk('s3')->url($filename);
    //     // dd($filename);
    //     return $filename;
    // }


    
    
    

    

}
