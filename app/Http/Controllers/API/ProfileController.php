<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Customer;
use App\Branch; 
use App\Employee;
use App\Dashboard; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;


class ProfileController extends Controller 
{
    public $successStatus = 200;

    public function __construct()
    {
        $this->common = new Commonuse();
    }


    /** 
     * activity menu get
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function profile(Request $request){

        $input['customer_id'] = Auth::user()->customer_id;
        $input['branch_id'] = Auth::user()->branch_id;
        $user_id = Auth::user()->id;

        $my_profile = DB::table('employees as e')->where('e.user_id', $user_id)
        ->leftjoin('designations as des', 'e.designation_id', 'des.id')
        ->leftjoin('employees as rh', 'e.reporting_head_id', 'rh.id')
        ->leftjoin('designations as rdes', 'rh.designation_id', 'rdes.id')
        ->select('e.first_name', 'e.last_name', 'des.designation_name', 'e.profile_pic', 'e.profile_pic_status', 'rh.first_name as rh_fname', 'rh.last_name as rh_lname', 'rh.profile_pic as rh_profile_pic', 'rdes.designation_name as rh_designation_name')
        ->first();

        $result['full_name'] = $my_profile->first_name." ".$my_profile->last_name;
        $result['designation'] = $my_profile->designation_name;
        
        $result['profile_pic'] = "https://qolphin-assets.s3.us-east-2.amazonaws.com/default_image.png";
        if($my_profile->profile_pic != null && $my_profile->profile_pic != ""){
            $result['profile_pic'] = $my_profile->profile_pic;
        }
        
        $result['is_profile_pic_approved'] = 0;
        if($my_profile->profile_pic_status == 1){
            $result['is_profile_pic_approved'] = 1;
        }     

        $result['rh_name'] = $my_profile->rh_fname." ".$my_profile->rh_lname;

        $result['rh_profile_pic'] = "https://qolphin-assets.s3.us-east-2.amazonaws.com/default_image.png";
        if($my_profile->rh_profile_pic != null && $my_profile->rh_profile_pic != ""){
            $result['rh_profile_pic'] = $my_profile->rh_profile_pic;
        }        
        $result['rh_designation'] = $my_profile->rh_designation_name;   

        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

    }


    /** 
     * Upload profile picture
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function upload_profile_pic(Request $request){

        $validator = Validator::make($request->all(), [ 
            'profile_pic' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>"Picture is required", 'code'=>400], 400);            
        }

        $input = $request->all();

        $input['customer_id'] = Auth::user()->customer_id;
        $input['branch_id'] = Auth::user()->branch_id;
        $user_id = Auth::user()->id;

        $profile_pic = $input['profile_pic'];

        $employees = DB::table('employees')->where('user_id', $user_id)->first();

        $employee_id = $employees->id;

        $approvals['user_id'] = $user_id;
        $approvals['employee_id'] = $employee_id;
        $approvals['approval_type'] = 'profile_pic';
        $approvals['approval_value'] = $profile_pic;    

        DB::table('profile_approval_logs')->where('employee_id', $employee_id)->update(['status'=>2]); 

        $res = DB::table('profile_approval_logs')->insert($approvals);

        DB::table('employees as e')->where('e.user_id', $user_id)->update(['profile_pic'=>$profile_pic, 'profile_pic_status'=>0]);

        $result['res'] = $res;

        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

    }
    
    
    

    

}
