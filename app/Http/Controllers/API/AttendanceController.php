<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Customer;
use App\Branch; 
use App\Employee;
use App\Dashboard; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;
use App\Regularizes;


class AttendanceController extends Controller 
{
    public $successStatus = 200;

    public function __construct()
    {
        $this->common = new Commonuse();
    }

    /** 
     * Employee checkin and checkout initialize,
     * AFTER SELFIE CLICK TO INITIALIZE THIS API
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function check_in_out_init(Request $request){
        $validator = Validator::make($request->all(), [ 
            // 'customer_id' => 'required',
            // 'branch_id' => 'required',
            // 'employee_id' => 'required',
            // 'user_id' => 'required',
            'log_type' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>"Log type is required", 'code'=>400], 400);            
        }

        $input = $request->all();
        $log_type = $input['log_type'];

        $branch_id = Auth::user()->branch_id; 
        $user_id = Auth::user()->id;
       // $employee_id = Auth::user()->employee_id;

        //get previleges
        $res = $this->common->branch_previleges($branch_id, $log_type, 1);

        $previleges = $res['previleges'];

        $result['previleges'] = $previleges; 

        $employee = DB::table('employees as emp')->join('shifts as s', 'emp.shift_id', 's.id')
        ->where('emp.user_id', $user_id)->where('emp.is_active', 1)->first();

        if($log_type == "CHECKIN"){
            $db_log_type = 'checkin';
            $time = $employee->start_time;
        }else{
            $db_log_type = 'checkout';
            $time = $employee->end_time;
        }

        $questions = array();

        // $question['question_id'] =3;
        // $question['question'] = "Reason for location mismatching";
        // $question['question_type'] = 'gps_match';
        // $question['answer_type'] = 'text_box';
        // $questions[] = $question;

        // $question['question_id'] = 1;
        // $question['question'] = "Reason for mismatching photo";
        // $question['question_type'] = 'face_match';
        // $question['answer_type'] = 'text_box';
        // $questions[] = $question;

        // $question['question_id'] = 2;
        // $question['question'] = "Reason for late ".$db_log_type;
        // $question['question_type'] = 'late';
        // $question['answer_type'] = 'text_box';
        // $questions[] = $question;

        if(array_key_exists($db_log_type.'_gps', $previleges)){

            if(!isset($input['latitude']) && !isset($input['longitude'])){
                return response()->json(['error'=>"Latitude and longitude is required", 'code'=>400], 400); 
            }

            $get_branch = DB::table('branches')->where('id', $branch_id)->first();
            $latitude = $get_branch->latitude;
            $longitude = $get_branch->longitude;

            $lat_lns[$db_log_type.'_lat'] = $check_lat = $input['latitude'];
            $lat_lns[$db_log_type.'_lng'] = $check_lng = $input['longitude'];

            $result[$db_log_type.'_gps'] = $lat_lns; 
            
            if(array_key_exists($db_log_type.'_question', $previleges)){

                $distance = DB::select("SELECT calculate_distance(".$latitude.", ".$longitude.", ".$check_lat.", ".$check_lng.", 'M')");
                if(isset($distance[0]->calculate_distance)){
                    $result[$db_log_type.'_distance'] = $distance[0]->calculate_distance;
                    if($distance[0]->calculate_distance > 200){
                        $question['question_id'] =3;
                        $question['question'] = "Reason for location mismatching";
                        $question['question_type'] = 'gps_match';
                        $question['answer_type'] = 'text_box';

                        $questions[] = $question;
                    }
                }

            }
        }
       
        
        $result[$db_log_type.'_time'] = $this->common->current_time();
        $result[$db_log_type.'_date'] = $this->common->current_date();
        $result['attendance_date'] = $this->common->current_date();
        
        $result[$db_log_type.'_face'] = 0;
        if(array_key_exists($db_log_type.'_selfie', $previleges)){

            //$selfie = $input[$db_log_type.'_selfie'];
            //$profile_pic = $employee->profile_pic;

            if(!isset($input['selfie'])){
                return response()->json(['error'=>"Selfie is required", 'code'=>400], 400); 
            }

            $selfie = $input['selfie']; //"https://prasannakodai.files.wordpress.com/2016/03/photo-1.jpg";//
            $profile_pic = $employee->profile_pic;

            $result[$db_log_type.'_selfie'] = $selfie;

            if(array_key_exists($db_log_type.'_face', $previleges)){
                $response = $this->common->compare_face_facex($selfie, $profile_pic);

                if(isset($response->confidence)){
                    $confidence = round($response->confidence, 2);

                    if($confidence == ""){
                        $confidence_io = 0;
                    }

                    $confidence_io = $confidence;

                    $result[$db_log_type.'_face'] = $confidence_io;

                    if(array_key_exists($db_log_type.'_question', $previleges)){
                        if( abs($confidence) >= abs(0.40)){
                            $question['question_id'] = 1;
                            $question['question'] = "Reason for mismatching photo";
                            $question['question_type'] = 'face_match';
                            $question['answer_type'] = 'text_box';
    
                            $questions[] = $question;
                        }
                    }
                    
                }
            }

        }

        $current_time = $this->common->current_time();

        if(strtotime($time) >= strtotime($current_time)){

            if($log_type == "CHECKOUT"){

                //late
                if(array_key_exists($db_log_type.'_question', $previleges)){
                    $question['question_id'] = 2;
                    $question['question'] = "Reason for early ".$db_log_type;
                    $question['question_type'] = 'late';
                    $question['answer_type'] = 'text_box';

                    $questions[] = $question;
        
                }
            
            }

        }else{

            if($log_type == "CHECKIN"){

                //late
                if(array_key_exists($db_log_type.'_question', $previleges)){
                    $question['question_id'] = 2;
                    $question['question'] = "Reason for late ".$db_log_type;
                    $question['question_type'] = 'late';
                    $question['answer_type'] = 'text_box';

                    $questions[] = $question;
        
                }
            
            }

        }

        $result[$db_log_type.'_question'] = $questions; 
        
        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 
        
    }

    /** 
     * Employee checkin and checkout submit
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function check_in_out_submit(Request $request){


        $validator = Validator::make($request->all(), [ 
            // 'customer_id' => 'required',
            // 'branch_id' => 'required',
            //'employee_id' => 'required',
            // 'user_id' => 'required',
            'log_type' => 'required',
            'attendance_date' => 'required',
        ]);
            //
        if ($validator->fails()) { 
            return response()->json(['error'=>"Log Type is required", 'code'=>400, 'dev_error'=>$validator->errors()], 400);            
        }

        $input = $request->all();
        $customer_id = Auth::user()->customer_id;
        $branch_id = Auth::user()->branch_id;
        $data['user_id'] = $user_id = Auth::user()->id;
        $log_type = $input['log_type'];

        $data['attendance_date'] = $input['attendance_date'];

        $emps = DB::table('employees')->where('user_id', $user_id)->first();
        if(!$emps){
            return response()->json(['error'=>'Get employee failed', 'code'=>400], 400);
        }
        $data['employee_id'] = $employee_id = $emps->id;

        if($log_type == "CHECKIN"){
            $db_log_type = 'checkin';
            $array = array(
                'checkin_time', 'checkin_date', 'checkin_distance'
            );
        }else{
            $db_log_type = 'checkout';
            $array = array(
                'checkout_time', 'checkout_date', 'checkout_distance'
            );
        }

        $input[$db_log_type] = 1;

        //get previleges
        $res = $this->common->branch_previleges($branch_id, $log_type, 1);

        $previleges = $res['previleges'];

        $error = array(); $error_count = 0;
        foreach ($previleges as $k => $v){
            if($v == 1){
                if(!isset($input[$k])){
                    $error[$k] = $k.' is required';
                    $error_count += 1;
                }
            }
        }

        if($error_count > 0){
            return response()->json(['error'=>$error, 'code'=>400], 400); 
        }

        $data[$db_log_type.'_time'] = $input[$db_log_type.'_time'];

        if(array_key_exists($db_log_type.'_gps', $previleges)){
            $data[$db_log_type.'_lat'] = $input[$db_log_type.'_gps'][$db_log_type.'_lat'];
            $data[$db_log_type.'_lng'] = $input[$db_log_type.'_gps'][$db_log_type.'_lng'];
        }

        if(array_key_exists($db_log_type.'_selfie', $previleges)){
            $data[$db_log_type.'_selfie'] = $input[$db_log_type.'_selfie'];
        }

        if(array_key_exists($db_log_type.'_face', $previleges)){
            $data[$db_log_type.'_face'] = $input[$db_log_type.'_face'];
        }

        if(isset($input[$db_log_type.'_question'])){
            $ques = $input[$db_log_type.'_question'];
        }else{
            return response()->json(['error'=>"questions in empty", 'code'=>400], 400);
        }

        //checkinout enabled flag insert
        $data['is_'.$db_log_type] = 1;

        $error = array(); $error_count = 0;
        foreach($array as $arr){
            if(!in_array($arr, $input)){
                $error[$arr] = $arr.' is required';
                $error_count += 1;
                $data[$arr] = $input[$arr];
            }
        }

        if($error_count > 0){
            return response()->json(['error'=>$error, 'code'=>400], 400); 
        }

        $attendance_configs = DB::table('attendance_configs')->where('branch_id', $branch_id)->first();
        
        if($attendance_configs){


            if($attendance_configs->is_auto_approve == 1){
                $data['attendance_status'] = 'P';
                $data['approval_status'] = 1;
            }elseif($attendance_configs->is_rh_approve == 1){
                $data['attendance_status'] = 'P';
                $data['approval_status'] = 0;
            }elseif($attendance_configs->is_rh_level_approve == 1){
                $data['attendance_status'] = 'P';
                $data['approval_status'] = 0;
            }else{
               // $data['attendance_status'] = 'P';
            }

            
        }else{
            return response()->json(['error'=>"Please set attendance configurations", 'code'=>400], 400);
        }

        if($log_type == "CHECKIN"){
            $attendance = DB::table('attendances')->insertGetId($data);
        }else{

            $get_attendance = DB::table('attendances')
            ->where('employee_id', $employee_id)->where('attendance_date', $data['attendance_date'])->first();

            if($get_attendance){
                DB::table('attendances')
                ->where('id', $get_attendance->id)
                ->update($data);

                $attendance = $get_attendance->id;
            }else{
                return response()->json(['error'=>"Checkin not avail", 'code'=>400], 400);
            }
        }

        

        $questions['user_id'] = $user_id;
        $questions['request_type_id'] = $attendance;    
        $questions['request_type'] = 'attendance';
        $questions['during_process'] = $db_log_type;

        //print_r($ques);exit;

        foreach ($ques as $q) {

            $questions['question'] = $q['question'];
            $questions['answer'] = $q['answer'];

            DB::table('type_question_answers')->insert($questions);
        }

        if($attendance_configs->is_rh_approve == 1 && $log_type == "CHECKIN"){

            $approve_data['attendance_id'] = $attendance;

            DB::table('attendance_approval_logs')->insert($approve_data);
            
        }

        $result['message'] = "Submitted successfully";

        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

    }


    public function my_attendances_web(Request $request){


        $validator = Validator::make($request->all(), [ 
            'month' => 'required',
            'year' => 'required',
            'employee_id' => 'required',
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>"Parameters month/year is required", 'code'=>400, 'dev_error'=>$validator->errors()], 400);            
        }

        $request_data = $request->all();

        if(isset($request_data['month']) && isset($request_data['year'])){

            $month = $request_data['month'];
            $year = $request_data['year'];

        }else{

            $year = date('Y');
            $month = date('m');
        }  

        $employee_id = $request_data['employee_id'];

        $emp = DB::table('employees')->where('id', $employee_id)->first();

        $user_id = $emp->user_id;

        $branch_id = $emp->branch_id;

        $between_date = $year.'-'.$month.'-15';

        $first_day_month = date('Y-m-01', strtotime($between_date));

        $last_day_month  = date('Y-m-t', strtotime($between_date)); 
        
        $get_date_series = DB::select("select date_series::date from generate_series(date '".$first_day_month."',date '".$last_day_month."','1 day') as date_series");

        $all_dates = array();
        foreach ($get_date_series as $date_series) {
                
            $date = $date_series->date_series;

            $attendances = DB::table('attendances as a')->where('a.user_id', $user_id)
            ->where('a.attendance_date', $date)
            ->first();

            $atts['start'] = $date;
            

            //attendance status = 1-present, 0-absent, 2-leave
            if($attendances){
                if($attendances->attendance_status == 'P' && $attendances->approval_status == 1){
                    $atts['title'] = 'Present';
                    $atts['color'] = 'green';
                }else if($attendances->attendance_status == 'P' && $attendances->approval_status == 0){
                    $atts['title'] = 'Present';
                    $atts['color'] = 'grey';
                }else if($attendances->attendance_status == 'L' && $attendances->approval_status == 1){
                    $atts['title'] = 'Leave';
                    $atts['color'] = 'green';
                }else if($attendances->attendance_status == 'L' && $attendances->approval_status == 0){
                    $atts['title'] = 'Leave';
                    $atts['color'] = 'grey';
                }else{
                    $atts['title'] = 'Absent';
                    $atts['color'] = 'yellow';
                }
            }else{

                $atts['title'] = 'Absent';
                $atts['color'] = 'yellow';

                if(date("m", strtotime($between_date)) == date("m")){

                    if($date <= date('Y-m-d')){

                    }else{

                        $atts['title'] = '';
                        $atts['color'] = '';

                    }

                }else{

                    if($date <= date('Y-m-d')){

                    }else{

                        $atts['title'] = '';
                        $atts['color'] = '';

                    }

                } 

                
            }

            $all_dates[] = $atts;
            
        }

        $holidays = DB::table('holidays')->where('branch_id', $branch_id)
        ->where('is_active', 1)
        ->get();

        if($holidays){

            foreach ($holidays as $holiday) {

                $atts['color'] = 'blue';
                $atts['title'] = $holiday->holiday_name;
                $atts['start'] = $holiday->holiday_date;

                $all_dates[] = $atts;

            }

        }

        //print_r($all_dates);exit;

        return response()->json(['success' => $all_dates, 'code'=>200], $this->successStatus);

    }


    public function my_attendances(Request $request){

        $validator = Validator::make($request->all(), [ 
            'month' => 'required',
            'year' => 'required',
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>"Parameters month/year is required", 'code'=>400, 'dev_error'=>$validator->errors()], 400);            
        }

        $request_data = $request->all();

        if(isset($request_data['month']) && isset($request_data['year'])  ){

            $month = $request_data['month'];
            $year = $request_data['year'];

        }else{

            $year = date('Y');
            $month = date('m');
        }  

        $user_id = Auth::user()->id;

        $branch_id = Auth::user()->branch_id;

        $between_date = $year.'-'.$month.'-15';

        $first_day_month = date('Y-m-01', strtotime($between_date));

        $last_day_month  = date('Y-m-t', strtotime($between_date)); 
        
        $get_date_series = DB::select("select date_series::date from generate_series(date '".$first_day_month."',date '".$last_day_month."','1 day') as date_series");

        $all_dates = array();
        foreach ($get_date_series as $date_series) {
                
            $date = $date_series->date_series;

            $attendances = DB::table('attendances as a')->where('a.user_id', $user_id)
            ->where('a.attendance_date', $date)
            ->first();

            $holidays = DB::table('holidays')->where('branch_id', $branch_id)
            ->where('holiday_date', $date)->where('is_active', 1)
            ->first();

            $dt_format_change = date('d M, Y', strtotime($date));

            $atts['attendance_date'] = $dt_format_change;

            if($holidays){
                $atts['holiday_status'] = 1;
                $atts['holiday_status_char'] = 'Yes';
                $atts['holiday_name'] = $holidays->holiday_name;
            }else{
                $atts['holiday_status'] = 0;
                $atts['holiday_status_char'] = 'No';
            }

            //attendance status = 1-present, 0-absent, 2-leave
            if($attendances){
                if($attendances->attendance_status == 'P' && $attendances->approval_status == 1){
                    $atts['attendance_status'] = 1;
                    $atts['attendance_status_char'] = 'Present';
                    $atts['approval_status'] = 1;
                    $atts['approval_status_char'] = 'Approved';
                }else if($attendances->attendance_status == 'P' && $attendances->approval_status == 0){
                    $atts['attendance_status'] = 1;
                    $atts['attendance_status_char'] = 'Present';
                    $atts['approval_status'] = 0;
                    $atts['approval_status_char'] = 'Not Approved';
                }else if($attendances->attendance_status == 'L' && $attendances->approval_status == 1){
                    $atts['attendance_status'] = 2;
                    $atts['attendance_status_char'] = 'Leave';
                    $atts['approval_status'] = 1;
                    $atts['approval_status_char'] = 'Approved';
                }else if($attendances->attendance_status == 'L' && $attendances->approval_status == 0){
                    $atts['attendance_status'] = 2;
                    $atts['attendance_status_char'] = 'Leave';
                    $atts['approval_status'] = 0;
                    $atts['approval_status_char'] = 'Not Approved';
                }else{
                    $atts['attendance_status'] = 0;
                    $atts['attendance_status_char'] = 'Absent';
                    $atts['approval_status'] = 0;
                    $atts['approval_status_char'] = 'No need to approve';
                }
            }else{

                $atts['attendance_status'] = 0;
                $atts['attendance_status_char'] = 'Absent';
                $atts['approval_status'] = 0;
                $atts['approval_status_char'] = 'No need to approve';

                if(date("m", strtotime($between_date)) == date("m")){

                    if($date <= date('Y-m-d')){

                    }else{

                        $atts['attendance_status'] = -1;
                        $atts['attendance_status_char'] = 'Future Date';
                        $atts['approval_status'] = -1;
                        $atts['approval_status_char'] = 'No action here';
                    }

                } 

                
            }

            $change_format_dt = date('d-m-Y', strtotime($date));

            $all_dates[$change_format_dt] = $atts;

            $attendances_dates = $all_dates;

        }

        $result['attendances'] = $attendances_dates;

        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus);

    }


    //regularizes for attendance date
    public function attendance_regularize(Request $request){

        $validator = Validator::make($request->all(), [ 
            'attendance_date' => 'required',
            'checkin_time' => 'required',
            'checkout_time' => 'required',
            'reason' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>"Please fill all fields", 'code'=>400, 'dev_error'=>$validator->errors()], 400);            
        }

        $request_data = $request->all();

        $request_data['user_id'] = $user_id = Auth::user()->id;

        $emps = DB::table('employees')->where('user_id', $user_id)->first();
        if(!$emps){
            return response()->json(['error'=>'Get employee failed', 'code'=>400], 400);
        }
        $employee_id = $emps->id;

        $emp = DB::table('employees')->where('id', $employee_id)->first();

        $request_data['employee_id'] = $emp->id;
        $request_data['attendance_status'] = 'P';
        $request_data['approval_status'] = 5;
        $request_data['is_checkin'] = 0;
        $request_data['is_checkout'] = 0;

        $reason = $request_data['reason'];

        unset($request_data['reason']);

        $attendance_id = DB::table('attendances')->insertGetId($request_data);

        $request_data['attendance_id'] = $attendance_id;

        $request_data['reason'] = $reason;

        $data = Regularizes::create($request_data);

        $success = $data;

        if($data){
            $code = 200;
        }else{
            $code = 400;
        }

        return response()->json(['success' => $success, 'code'=>$code], $code);
    }
    

    

}
