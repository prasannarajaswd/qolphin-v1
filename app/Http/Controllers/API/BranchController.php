<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Customer;
use App\Branch; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;
use App\BranchPrevileges;
use App\AttendanceConfigs;


class BranchController extends Controller 
{
    public $successStatus = 200;

    public function __construct()
    {
        $this->common = new Commonuse();
    }


    /** 
     * Branch add api
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function branch_add(Request $request){

        if(Auth::check()){

        }else{
            return response()->json(['error'=>'Authorization failed', 'code'=>400], 200);  
        }

        $input = $request->all(); 
 
        $branch_data = $input['values'];

        $branch_data['cin'] = strtoupper($branch_data['cin']);
        $branch_data['customer_id'] = Auth::user()->id;

        $branch = Branch::create($branch_data);

        $previleges = DB::table('previleges')->where('is_active', 1)->count();

        for ($i=0; $i < $previleges; $i++) { 

            $k = $i+1;

            $bp['branch_id'] = $branch->id;
            $bp['customer_id'] = Auth::user()->id;
            $bp['previlege_id'] = $k;
            $bp['answer'] = $branch_data['previlege_id_'.$k];

            if(isset($bp) && count($bp) > 0){
                BranchPrevileges::create($bp);
            }   

        }

        $result['branch'] = $branch;

        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

    }

    /** 
     * Branch add api
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function branch_update(Request $request){

        if(Auth::check()){

        }else{
            return response()->json(['error'=>'Authorization failed', 'code'=>400], 200);  
        }

        $input = $request->all(); 
 
        $branch_data = $input['values'];
        $branch_id = $input['id'];

        $branch_update = Branch::findOrFail($branch_id);
        $branch_update->update($branch_data);

        DB::table('branch_previleges')->where('branch_id', $branch_id)->update(['is_active'=>0]);

        $previleges = DB::table('previleges')->where('is_active', 1)->count();

        for ($i=0; $i < $previleges; $i++) { 

            $k = $i+1;

            $bp['branch_id'] = $branch_id;
            $bp['customer_id'] = Auth::user()->id;
            $bp['previlege_id'] = $k;
            $bp['answer'] = $branch_data['previlege_id_'.$k];

            if(isset($bp) && count($bp) > 0){
                BranchPrevileges::create($bp);
            }   

        }

        $result['branch'] = $branch_id;

        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

    }
    
    
    /** 
     * Branch add api
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function attendance_config(Request $request){

        if(Auth::check()){

        }else{
            return response()->json(['error'=>'Authorization failed', 'code'=>400], 200);  
        }

        $input = $request->all(); 
 
        $branch_config = $input['values'];

        $branch_config['customer_id'] = Auth::user()->id;
        $branch_config['branch_id'] = $input['id'];

        $get_configs = DB::table('attendance_configs')->where('branch_id', $input['id'])->first();

        if($get_configs){
            $config = AttendanceConfigs::findOrFail($get_configs->id);

            $config->update($branch_config);
        }else{
            $config = AttendanceConfigs::create($branch_config); 
        }   
        
        $result['branch'] = $config;

        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

    }

    /**
     * Show the application designation.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function designation_add(Request $request)
    {
        $requestData = $request->all();

        $data = $requestData['values'];
        $data['branch_id'] = $requestData['branch_id'];

        $result = DB::table('designations')->insert($data);

        $success = "";
        if($result){

            $success = $result;
            $code = 200;
        }else{
            $code = 200;
        }

        return response()->json(['success' => $success, 'code'=>$code], $this->successStatus); 

    }

    /**
     * Show the application designation.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function designation_update(Request $request)
    {
        $requestData = $request->all();

        $data = $requestData['values'];
        $id = $requestData['value_id'];


        $result = DB::table('designations')->where('id', $id)->update($data);

        $success = "";
        if($result){

            $success = $result;
            $code = 200;
        }else{
            $code = 200;
        }

        return response()->json(['success' => $success, 'code'=>$code], $this->successStatus); 

    }


    /**
     * Show the application shift.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function shift_add(Request $request)
    {
        $requestData = $request->all();

        $data = $requestData['values'];
        $data['branch_id'] = $requestData['branch_id'];

        $result = DB::table('shifts')->insert($data);

        $success = "";
        if($result){

            $success = $result;
            $code = 200;
        }else{
            $code = 200;
        }

        return response()->json(['success' => $success, 'code'=>$code], $this->successStatus); 

    }

    /**
     * Show the application shift.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function shift_update(Request $request)
    {
        $requestData = $request->all();

        $data = $requestData['values'];
        $id = $requestData['value_id'];


        $result = DB::table('shifts')->where('id', $id)->update($data);

        $success = "";
        if($result){

            $success = $result;
            $code = 200;
        }else{
            $code = 200;
        }

        return response()->json(['success' => $success, 'code'=>$code], $this->successStatus); 

    }

    /**
     * Show the application Holiday.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function holiday_add(Request $request)
    {
        $requestData = $request->all();
        
        $data = $requestData['values'];

        $data['holiday_date'] = date('Y-m-d', strtotime($data['holiday_date']));

        $data['branch_id'] = $requestData['branch_id'];

       // print_r($data);exit;

        $result = DB::table('holidays')->insert($data);

        $success = "";
        if($result){

            $success = $result;
            $code = 200;
        }else{
            $code = 200;
        }

        return response()->json(['success' => $success, 'code'=>$code], $this->successStatus); 

    }

    /**
     * Show the application Holiday.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function holiday_update(Request $request)
    {
        $requestData = $request->all();

        $data = $requestData['values'];
        $id = $requestData['value_id'];

        $data['holiday_date'] = date('Y-m-d', strtotime($data['holiday_date']));

        $result = DB::table('holidays')->where('id', $id)->update($data);
        // print_r($result);
        // $res = DB::table('holidays')->where('id', $id)->get();
        // print_r($res);
        // exit;


        $success = "";
        if($result){

            $success = $result;
            $code = 200;
        }else{
            $code = 200;
        }

        return response()->json(['success' => $success, 'code'=>$code], $this->successStatus); 

    }

    /**
     * Show the application get_branch_configs.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function get_branch_configs(Request $request)
    {
        $requestData = $request->all();

        $data = $requestData['values'];

        $branch_id = $data['branch_id'];

        $result['shifts'] = DB::table('shifts')->where('branch_id', $branch_id)->get();
        $result['designations'] = DB::table('designations')->where('branch_id', $branch_id)->get();
        $result['rh'] = DB::table('employees')->where('branch_id', $branch_id)
        ->where('is_reporting_head', 1)->get();

        $success = "";
        if($result){

            $success = $result;
            $code = 200;
        }else{
            $code = 200;
        }

        return response()->json(['success' => $success, 'code'=>$code], $this->successStatus); 

    }

    

}
