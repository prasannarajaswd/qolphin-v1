<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;

class HomeController extends Controller 
{
    public $successStatus = 200;


    /** 
     * Home get api
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function home(Request $request){

        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required',
            'user_type' => 'required',
            'customer_id' => 'required',
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }

        $input = $request->all(); 
        $user_id = $input['user_id']; 



    }
    
    
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(Request $request){
        
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required',
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }
        
        $input = $request->all(); 
        $mobile = $input['mobile']; 
        
        $get_users = DB::table('users')->where('mobile', $mobile)->first();

        if($get_users){

            if($get_users->is_verify_otp == "0"){

                //send otp
                $user_id = $get_users->id;
                $otp = '1234';
                $result['otp'] = $otp;
                $result['mobile'] = $mobile;
                $result['new_register'] = 'yes';
                $result['next_screen'] = 'otp';
                $result['user_id'] = $user_id;

                DB::table('users')->where('id', $user_id)->update(['otp'=>$otp]);

            }else{

                $result['new_reg'] = 'no';
                

                //qpin_enabled check
                $is_qpin_enabled = 1;
                if($is_qpin_enabled == 1){
                    $result['next_screen'] = 'qpin';
                }else{
                    $result['next_screen'] = 'home';
                }

                $result['user'] = $get_users;

            }

            $success = $result;

            return response()->json(['success' => $success], $this->successStatus); 

        }else{
            return response()->json(['error'=>"mobile number not registered"], 400); 
        }

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            //$success['token'] =  $user->createToken('MyApp')->accessToken; 
            $success['user'] =  $user; 
            return response()->json(['success' => $success], $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }


    /** 
     * verify otp 
     * 
     * @return \Illuminate\Http\Response 
    */ 
    public function verify_otp(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'otp' => 'required', 
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }

        $input = $request->all(); 
        $otp = $input['otp'];
        $user_id = $input['user_id']; 
        
        $get_users = DB::table('users')
        ->where('id', $user_id)->where('otp', $otp)
        ->first();

        if($get_users){

            $result['new_reg'] = 'no';

            //qpin_enabled check
            $is_qpin_enabled = 1;
            if($is_qpin_enabled == 1){
                $result['next_screen'] = 'qpin_set';
            }else{
                $result['next_screen'] = 'home';
            }

            DB::table('users')->where('id', $user_id)->update(['is_verify_otp'=>1]);

            $result['user'] = $get_users;

            $success = $result;

            return response()->json(['success' => $success], $this->successStatus);

        }else{
            return response()->json(['error'=>'OTP verification is failed'], 400); 
        }

    }


    /** 
     * qpin set 
     * 
     * @return \Illuminate\Http\Response 
    */ 
    public function qpin_set(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'qpin' => 'required', 
            'c_qpin' => 'required|same:qpin', 
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }

        $input = $request->all(); 
        $qpin = $input['qpin'];
        $user_id = $input['user_id']; 
        
        $set_qpin = DB::table('users')
        ->where('id', $user_id)->update(['qpin'=>$qpin]);

        if($set_qpin){

            $result['new_reg'] = 'no';

            $get_users = DB::table('users')
            ->where('id', $user_id)
            ->first();

            $result['user'] = $get_users;

            $success = $result;

            return response()->json(['success' => $success], $this->successStatus);

        }else{
            return response()->json(['error'=>'QPIN set failed'], 400); 
        }

    }


    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email|unique:users', 
            'mobile' => 'required|unique:users',
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
                
        $user = User::create($input); 
        //print_r($user);
        //exit;
        //$success['token'] =  $user->createToken('MyApp')->accessToken; 
        $success['name'] =  $user->name;
        return response()->json(['success'=>$success], $this->successStatus); 
    }


    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this->successStatus); 
    } 

}
