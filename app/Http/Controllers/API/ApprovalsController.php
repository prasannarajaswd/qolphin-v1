<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Customer;
use App\Branch; 
use App\Employee;
use App\Dashboard; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;
use App\Regularizes;


class ApprovalsController extends Controller 
{
    public $successStatus = 200;

    public function __construct()
    {
        $this->common = new Commonuse();
    }


    /** 
     * Employee checkin and checkout initialize,
     * AFTER SELFIE CLICK TO INITIALIZE THIS API
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function approvals_list(Request $request){
        $validator = Validator::make($request->all(), [ 
            'approval_type' => 'required',
            'approval_status' => 'required',
            'approval_request' => 'required',
        ]); 

        //'approval_type' attendance, leave, comp-off

        //'approval_status' 0-pending, 1-approved, 2-rejecteed

        //'approval_request' tl-reporting head, emp - employee

        if ($validator->fails()) { 
            return response()->json(['error'=>"Approval Type / Approval Status is required", 'code'=>400, 'dev_error'=>$validator->errors()], 400);            
        }

        $input = $request->all();

        $this->approval_status = $input['approval_status'];
        $this->approval_request = $input['approval_request'];

        if($input['approval_type'] == "attendance"){

            $res = $this->attendance();

        }elseif($input['approval_type'] == "profile"){

            $res = $this->profile();

        }elseif($input['approval_type'] == "regularize"){
            
            $res = $this->regularize();

        }else{
            return response()->json(['success' => "Not valid approval type", 'code'=>400], 400); 
        }

        return response()->json(['success' => $res, 'code'=>200], 200); 
    }

    public function attendance(){

        $user_id = Auth::user()->id;

        $employee = DB::table('employees')->where('user_id', $user_id)->first();

        $employee_id = $employee->id;

        $employees = DB::table('employees')->where('reporting_head_id', $employee_id)->pluck('id')->toArray();

        $responses = array();
        if(count($employees) > 0){

            $list_emps = DB::table('attendances as att')->whereIn('att.employee_id', $employees)
            ->where('att.approval_status', $this->approval_status)
            ->join('employees as emp', 'att.employee_id', 'emp.id')
            ->join('attendance_approval_logs as aal', 'att.id', 'aal.attendance_id')
            ->select('emp.id as employee_id', 'emp.profile_pic', 'emp.first_name','emp.last_name', 'aal.id as approval_id', 'att.*')
            ->orderby('att.attendance_date' , 'desc')
            ->get();

            $checkin = 1; $checkout = 0;
            foreach ($list_emps as $list_emp) {
                $res['approval_id'] = $list_emp->approval_id;

                if($this->approval_status == 0 && $this->approval_request == "tl" && $list_emp->is_checkin == 1 && $list_emp->is_checkout == 1){

                    $btn['accept_button'] = 1;
                    $btn['decline_button'] = 1;
                    $btn['cancel_button'] = 0;

                }else{
                    $btn['accept_button'] = 0;
                    $btn['decline_button'] = 0;
                    $btn['cancel_button'] = 0;
                }

                if($list_emp->is_checkin == 1 && $list_emp->is_checkout == 0){
                    $title_1['title_1_1'] = "In Done";
                    $title_1['title_1_2'] = "Out Pending";
                    $res['color_combination'] = "combination_pending";
                }

                if ($list_emp->is_checkin == 1 && $list_emp->is_checkout == 1) {
                    $title_1['title_1_1'] = "In Done";
                    $title_1['title_1_2'] = "Out Done";
                    $res['color_combination'] = "combination_done";
                }

                if($this->approval_status == 2){
                    $res['color_combination'] = "combination_declined";
                }    

                $res['checkin'] = $list_emp->is_checkin;  
                $res['checkout'] = $list_emp->is_checkout;    

                $image = "https://qolphin-assets.s3.us-east-2.amazonaws.com/default_image.png";
                if($list_emp->profile_pic != "" ){
                    $image = $list_emp->profile_pic;
                }

                $res['image'] = $image;
                $res['title_1'] = $title_1;
                $res['title_2'] = date('d M, Y', strtotime($list_emp->attendance_date));
                $res['title_3'] = $list_emp->first_name.' '.$list_emp->last_name;
                $res['button'] = $btn;
                $responses[] = $res;
            }


        }        

        return $responses;

        
    }

    

    /** 
     * approval_action
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function approval_action(Request $request){


        $validator = Validator::make($request->all(), [ 
            'approval_code' => 'required',
            'approval_id' => 'required',
            'approval_type' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>"Approval-code/id/type is required", 'code'=>400, 'dev_error'=>$validator->errors()], 400);            
        }

        $input = $request->all();
        $user_id = Auth::user()->id;
        $customer_id = Auth::user()->customer_id;

        $ac = $input['approval_code'];
        $ai = $input['approval_id'];
        $at = $input['approval_type'];

        if($ac == 1){
            $success = "Approved";
        }else{
            $success = "Rejected";
        }

        if($at == "attendance"){

            $approvals = DB::table('attendance_approval_logs')->where('id', $ai)->first();

            if($approvals){

                $res = DB::table('attendance_approval_logs')->where('id', $ai)->update([
                    'checkin_approval' => $ac, 'checkin_approved_by' => $user_id, 
                    'checkin_approved_date' => date('Y-m-d'),'checkout_approval' => $ac, 
                    'checkout_approved_by' => $user_id, 'checkout_approved_date' => date('Y-m-d')
                ]);

                $attendance_id = $approvals->attendance_id;

                DB::table('attendances')->where('id', $attendance_id)->update(['approval_status'=>$ac]);

            }else{
                return response()->json(['error'=>"Not in approvals", 'code'=>400], 400);     
            }

        }

        if($at == "profile"){

            $profiles = DB::table('profile_approval_logs as pal')
            ->where('pal.id', $ai)->first();

            if(!$profiles){
                return response()->json(['error'=>"Not in approvals", 'code'=>400], 400); 
            }

            $employee_id = $profiles->employee_id;

            DB::table('profile_approval_logs')->where('id', $ai)->update(['status'=>$ac]);

            if($ac == 2){
                DB::table('employees')->where('id', $employee_id)
                ->update(['profile_pic_status'=>0, 'profile_pic'=>""]);
            }else{
                DB::table('employees')->where('id', $employee_id)->update(['profile_pic_status'=>$ac]);
            }

            if($ac == 1){
                $success = "Approved";
            }else{
                $success = "Rejected";
            }

        }

        if($at == "regularize"){

            $regularizes = DB::table('regularizes')
            ->where('id', $ai)->first();

            if(!$regularizes){
                return response()->json(['error'=>"Not in approvals", 'code'=>400], 400); 
            }

            //$employee_id = $regularizes->employee_id;
            $attendance_id = $regularizes->attendance_id;

            if($ac == 1){

                DB::table('regularizes')->where('id', $ai)->update(['approval_status'=>1]);

                DB::table('attendances')->where('id', $attendance_id)->update(['approval_status'=>1]);

                $success = "Approved";

            }else{
                DB::table('regularizes')->where('id', $ai)->update(['approval_status'=>2]);

                $success = "Rejected";
            }
                        

        }


       return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 
        

    }


    public function approval_view(Request $request){

        $validator = Validator::make($request->all(), [ 
            'approval_id' => 'required',
            'approval_type' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>"Approval type / Approval id is required", 'code'=>400, 'dev_error'=>$validator->errors()], 400);            
        }

        $input = $request->all();
        $user_id = Auth::user()->id;
        $customer_id = Auth::user()->customer_id;

        $ai = $input['approval_id'];
        $at = $input['approval_type'];

        if($at == 'attendance'){
            $result = $this->attendance_approval_view($ai);
        }

        if($at == 'regularize'){
            $result = $this->regularize_approval_view($ai);
        }

        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

    }


    public function attendance_approval_view($ai){
        $approvals = DB::table('attendances as att')
            ->join('employees as emp', 'att.employee_id', 'emp.id')
            ->leftjoin('designations as des', 'emp.designation_id', 'des.id')
            ->join('attendance_approval_logs as aal', 'att.id', 'aal.attendance_id')
            ->select('emp.id as employee_id', 'emp.profile_pic', 'emp.first_name','emp.last_name', 'aal.id as approval_id', 'att.*', 'des.designation_name', 'emp.emp_mobile')
            ->where('aal.id' , $ai)
            ->orderby('att.attendance_date' , 'desc')
            ->first();

        if(!$approvals){
            return response()->json(['error'=>"Approval is empty", 'code'=>400], 400); 
        }

        $result['approval_id'] = $ai;
        $result['profile_pic'] = "https://qolphin-assets.s3.us-east-2.amazonaws.com/default_image.png";
        if($approvals->profile_pic != "" ){
            $result['profile_pic'] = $approvals->profile_pic;
        }
       

        $result['display_name'] = $approvals->first_name.' '.$approvals->last_name;
        $result['designation_name'] = $approvals->designation_name;
        $result['mobile'] = $approvals->emp_mobile;
        $result['attendance_date'] = date('d M, Y', strtotime($approvals->attendance_date));

        if($approvals->approval_status == 0 && $approvals->is_checkin == 1 && $approvals->is_checkout == 1){
            $btn['accept_button'] = 1;
            $btn['decline_button'] = 1;
            $btn['cancel_button'] = 0;
        }else{
            $btn['accept_button'] = 0;
            $btn['decline_button'] = 0;
            $btn['cancel_button'] = 0;
        }

        $result['button'] = $btn;
        $result['is_checkin'] = $approvals->is_checkin;
        $result['is_checkout'] = $approvals->is_checkout;
        $result['is_check_in_out'] = 0;
        if($approvals->is_checkin == 1 && $approvals->is_checkout == 1){
            $result['is_check_in_out'] = 1;
        }
        



        //checkin questions
        $inquestion['q_title'] = "Selfie";
        $inquestion['q_answer'] = $approvals->checkin_selfie;
        $inquestion['q_type'] = "image";
        $inquestions[] = $inquestion;

        $inquestion['q_title'] = "Checkin Time";
        $inquestion['q_answer'] = date('H:i A', strtotime($approvals->checkin_time));
        $inquestion['q_type'] = "text";
        $inquestions[] = $inquestion;

        $get_inquestions = DB::table('type_question_answers')
        ->where('request_type', 'attendance')
        ->where('during_process', 'checkin')
        ->where('request_type_id', $approvals->id)
        ->get();

        if(count($get_inquestions) > 0){

            foreach ($get_inquestions as $get_inquestion) {
                $inquestion['q_title'] = $get_inquestion->question;
                $inquestion['q_answer'] = $get_inquestion->answer;
                $inquestion['q_type'] = "text";
                $inquestions[] = $inquestion;
            }

        }
        $res[]['checkin'] = $inquestions;

        //checkout questions
        $outquestion['q_title'] = "Selfie";
        $outquestion['q_answer'] = $approvals->checkout_selfie;
        $outquestion['q_type'] = "image";
        $outquestions[] = $outquestion;

        $outquestion['q_title'] = "Checkout time";
        $outquestion['q_answer'] = date('H:i A', strtotime($approvals->checkout_time));
        $outquestion['q_type'] = "text";
        $outquestions[] = $outquestion;

        $get_outquestions = DB::table('type_question_answers')
        ->where('request_type', 'attendance')
        ->where('during_process', 'checkout')
        ->where('request_type_id', $approvals->id)
        ->get();

        if(count($get_outquestions) > 0){

            foreach ($get_outquestions as $get_outquestion) {
                $outquestion['q_title'] = $get_outquestion->question;
                $outquestion['q_answer'] = $get_outquestion->answer;
                $outquestion['q_type'] = "text";
                $outquestions[] = $outquestion;
            }
                        
        }
        $res[]['checkout'] = $outquestions;

        $result['questions'] = $res;

        return $result;
    }

    public function regularize_approval_view($ai){

        $approvals = DB::table('attendances as att')
            ->join('employees as emp', 'att.employee_id', 'emp.id')
            ->leftjoin('designations as des', 'emp.designation_id', 'des.id')
            ->join('regularizes as reg', 'att.id', 'reg.attendance_id')
            ->select('emp.id as employee_id', 'emp.profile_pic', 'emp.first_name','emp.last_name', 'reg.id as approval_id', 'att.*', 'des.designation_name', 'emp.emp_mobile', 'reg.reason')
            ->where('aal.id' , $ai)
            ->orderby('att.attendance_date' , 'desc')
            ->first();

        if(!$approvals){
            return response()->json(['error'=>"Approval is empty", 'code'=>400], 400); 
        }

        $result['approval_id'] = $ai;
        $result['profile_pic'] = "https://qolphin-assets.s3.us-east-2.amazonaws.com/default_image.png";
        if($approvals->profile_pic != "" ){
            $result['profile_pic'] = $approvals->profile_pic;
        }
       

        $result['display_name'] = $approvals->first_name.' '.$approvals->last_name;
        $result['designation_name'] = $approvals->designation_name;
        $result['mobile'] = $approvals->emp_mobile;
        $result['attendance_date'] = date('d M, Y', strtotime($approvals->attendance_date));

        $btn['accept_button'] = 1;
        $btn['decline_button'] = 1;
        $btn['cancel_button'] = 0;

        $result['button'] = $btn;
        
        //checkin questions
        $question['q_title'] = "Reason";
        $question['q_answer'] = $approvals->reason;
        $question['q_type'] = "text";
        $res[] = $question;        

        $result['questions'] = $res;
    }


    public function profile(){

        if($this->approval_status == 0 && $this->approval_request == "tl"){

            $btn['accept_button'] = 1;
            $btn['decline_button'] = 1;
            $btn['cancel_button'] = 0;

        }

        $user_id = Auth::user()->id;

        $employee = DB::table('employees')->where('user_id', $user_id)->first();

        $employee_id = $employee->id;

        $employees = DB::table('employees')->where('reporting_head_id', $employee_id)->pluck('id')->toArray();

        $responses = array();
        if(count($employees) > 0){

            $profiles = DB::table('profile_approval_logs as pal')
            ->whereIn('pal.employee_id', $employees)
            ->where('pal.approval_type', 'profile_pic')
            ->where('pal.status', 0)
            ->join('employees as emp', 'pal.employee_id', 'emp.id')
            ->select('emp.*', 'pal.id as approval_id')
            ->get();

            $responses = array();
            if(count($profiles) > 0){

                foreach ($profiles as $profile) {
                    $res['approval_id'] = $profile->approval_id;
                    
                    $image = "https://qolphin-assets.s3.us-east-2.amazonaws.com/default_image.png";
                    if($profile->profile_pic != "" ){
                        $image = $profile->profile_pic;
                    }

                    $res['image'] = $image;
                    $res['title_1'] = "Profile Picture";
                    $res['title_2'] = date('d M, Y', strtotime($profile->created_at));
                    $res['title_3'] = $profile->first_name.' '.$profile->last_name;
                    $res['button'] = $btn;
                    $responses[] = $res;
                }

            }

            
        }        

        return $responses;

        
    }


    public function regularize(){

        if($this->approval_request == "tl"){

            $btn['accept_button'] = 1;
            $btn['decline_button'] = 1;
            $btn['cancel_button'] = 0;

        }

        $user_id = Auth::user()->id;

        $employee = DB::table('employees')->where('user_id', $user_id)->first();

        $employee_id = $employee->id;

        $employees = DB::table('employees')->where('reporting_head_id', $employee_id)->pluck('id')->toArray();

        $responses = array();
        if(count($employees) > 0){

            $list_emps = DB::table('attendances as att')
            ->whereIn('att.employee_id', $employees)
            ->where('att.approval_status', 5)
            ->join('employees as emp', 'att.employee_id', 'emp.id')
            ->join('regularizes as reg', 'att.id', 'reg.attendance_id')
            ->where('reg.approval_status',5)
            ->select('emp.id as employee_id', 'emp.profile_pic', 'emp.first_name','emp.last_name', 'reg.id as approval_id', 'att.*', 'reg.reason')
            ->orderby('att.attendance_date' , 'desc')
            ->get();

            foreach ($list_emps as $list_emp) {
                $res['approval_id'] = $list_emp->approval_id;
                $image = "https://qolphin-assets.s3.us-east-2.amazonaws.com/default_image.png";
                if($list_emp->profile_pic != "" ){
                    $image = $list_emp->profile_pic;
                }
                $res['image'] = $image;
                $res['title_1'] = "Regularize";  
                $res['title_2'] = date('d M, Y', strtotime($list_emp->attendance_date));
                $res['title_3'] = $list_emp->first_name.' '.$list_emp->last_name;
                $res['button'] = $btn;

                $res['chekin_time'] = $list_emp->checkin_time;
                $res['chekout_time'] = $list_emp->checkout_time;
                $res['reason'] = $list_emp->reason;

                $responses[] = $res;
            }

            
        }        

        return $responses;

        
    }

    
    

    

}
