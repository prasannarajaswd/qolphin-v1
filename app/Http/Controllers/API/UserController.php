<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;
use App\UserChannel;

class UserController extends Controller 
{
    public $successStatus = 200;

    public function __construct()
    {
        $this->common = new Commonuse();
    }

    /** 
     * cin_validation api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function cin_validation(Request $request){

        $validator = Validator::make($request->all(), [ 
            'cin' => 'required',
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>"Please Enter CIN", 'code'=>400, "dev_error"=>$validator->errors()], 400);            
        }
        
        $input = $request->all(); 
        $cin = $input['cin']; 

        $branches = DB::table('branches')->where('cin', $cin)->first();

        if($branches){

            $cin = $branches->cin;
            $customer_id = $branches->customer_id;
            $branch_id = $branches->id;

            $result['cin'] = $cin;
            $result['customer_id'] = $customer_id;
            $result['branch_id'] = $branch_id;

            $success = $result;

            return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

        }else{
            return response()->json(['error'=>"CIN Number Not Registered", 'code'=>400], 400);  
        }


    }
    
    
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(Request $request){
        
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required',
            'customer_id' => 'required',
            'branch_id' => 'required',
        ]);

        if ($validator->fails()) { 

            return response()->json(['error'=>"Please Enter Your Mobile Number", 'code'=>400, "dev_error"=>$validator->errors()], 400);       

        }
        
        $input = $request->all(); 
        $mobile = $input['mobile']; 
        $branch_id = $input['branch_id']; 
        $customer_id = $input['customer_id']; 
        
        $get_users = User::where('mobile', $mobile)->where('branch_id', $branch_id)
        ->where('customer_id', $customer_id)->first();

        if($get_users){

            $user_type = $get_users->user_type;

            if($get_users->user_type == "C"){
                $error = 1;
                $error_msg = "Not avail login";

            }elseif($get_users->user_type == "E"){

                //send otp
                $user_id = $get_users->id;  

                $otp = rand(1000, 9999);
                $result['otp'] = $otp;
                $result['mobile'] = $num = $mobile;
                $next_screen = 'otp'; 

                DB::table('users')->where('id', $user_id)->update(['otp'=>$otp]);

                $mes = "Please use this OTP: ".$otp." for qolphin account access.";  

                $this->common->send_sms($mes, $num);

                $result['next_screen'] = $next_screen;

                $result['user_id'] = $user_id;
    
                $success = $result;
                
                $error = 0;

            }elseif($get_users->user_type == "B"){
                $error = 1;
                $error_msg = "Not avail login";

            }elseif($get_users->user_type == " " || $get_users->user_type == null ){
                $error = 1;
                $error_msg = "Not avail login";

            }else{
                $error = 1;
                $error_msg = "Not avail login";
                
            }

            if($error == 1){
                return response()->json(['error'=> $error_msg, 'code'=>400], 400); 

            }else{
                return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

            }            

        }else{
            return response()->json(['error'=>"mobile number not registered", 'code'=>400], 400); 

        }

    }


    /** 
     * verify otp 
     * 
     * @return \Illuminate\Http\Response 
    */ 
    public function verify_otp(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_id' => 'required', 
            'otp' => 'required', 
        ]);

        if ($validator->fails()) { 
            return response()->json(['dev_error'=>$validator->errors(), 'error'=>"Enter Your OTP", 'code'=>400], 400);            
        }

        $input = $request->all(); 
        $otp = $input['otp'];
        $user_id = $input['user_id']; 
        
        $get_users = User::where('id', $user_id)->where('otp', $otp)
        ->first();

        if($get_users){

            $result['new_reg'] = 'no';
            $result['token'] = '';

            //qpin_enabled check
            $is_qpin_enabled = 1;
            if($is_qpin_enabled == 1){
                
                // if($get_users->qpin != "" && $get_users->qpin != null){
                //     $result['next_screen'] = 'qpin';
                // }else{
                //     $result['next_screen'] = 'qpin_set';
                // }

                $result['next_screen'] = 'qpin_set';
                
            }else{
                $result['next_screen'] = 'home';
            }

            DB::table('users')->where('id', $user_id)->update(['is_verify_otp'=>1]);

            //$result['user'] = $get_users;

            Auth::login($get_users);
            if (Auth::user()){
                $user = Auth::user();

                if(isset($input['platform']) && isset($input['device_id']) ){
                    $input['channel'] = "qol-".$input['device_id'];
                    UserChannel::where('user_id', $user_id)->update(['is_active'=>0]);
                    UserChannel::create($input);
                }

                $auths['header_1'] = 'Authorization';
                $auths['header_1_value'] = 'Bearer'.' '.$user->createToken('MyApp')->accessToken;
                $auths['header_2'] = 'Accept';
                $auths['header_2_value'] = 'application/json';

                $result['token'] = $user->createToken('MyApp')->accessToken;
                
                $result['headers'] =  $auths; 
            }else{
                return response()->json(['error'=>'Unauthorised', 'code'=>401], 401);
            }

            $success = $result;

            return response()->json(['success' => $success, 'code'=>200], $this->successStatus);

        }else{
            return response()->json(['error'=>'OTP verification is failed', 'code'=>400], 400); 
        }

    }


    /** 
     * qpin set 
     * 
     * @return \Illuminate\Http\Response 
    */ 
    public function qpin_set(Request $request){
        $validator = Validator::make($request->all(), [ 
            // 'user_id' => 'required', 
            'qpin' => 'required', 
            //'c_qpin' => 'required|same:qpin', 
        ]);

        if ($validator->fails()) { 
            return response()->json(['dev_error'=>$validator->errors(), 'error'=>"Please Enter QPIN", 'code'=>400], 400);            
        }

        // $res = $this->common->check_auth();
        // print_r($res);exit;

        $input = $request->all(); 
        $qpin = $input['qpin'];
        $user_id = Auth::user()->id;//$input['user_id']; 
        
        $set_qpin = DB::table('users')
        ->where('id', $user_id)->update(['qpin'=>$qpin]);

        if($set_qpin){

            $result['new_reg'] = 'no';

            $get_users = DB::table('users')
            ->where('id', $user_id)
            ->first();

            $result['user'] = $get_users;

            $success = $result;

            return response()->json(['success' => $success, 'code'=>200], $this->successStatus);

        }else{
            return response()->json(['error'=>'QPIN set failed', 'code'=>400], 400); 
        }

    }


    /** 
     * qpin verify 
     * 
     * @return \Illuminate\Http\Response 
    */ 
    public function qpin_verify(Request $request){
        $validator = Validator::make($request->all(), [ 
            //'user_id' => 'required', 
            'qpin' => 'required', 
        ]);

        if ($validator->fails()) { 
            return response()->json(['dev_error'=>$validator->errors(), 'error'=>"Enter Your QPIN", 'code'=>400], 400);            
        }

        $input = $request->all(); 
        $qpin = $input['qpin'];
        $user_id = Auth::user()->id; 
        
        $get_qpin = DB::table('users')->where('is_verify_otp', 1)
        ->where('id', $user_id)->where('qpin', $qpin)->first();

        if($get_qpin){

            $result['new_reg'] = 'no';

            $get_users = DB::table('users')
            ->where('id', $user_id)
            ->first();

            $result['user'] = $get_users;

            $success = $result;

            return response()->json(['success' => $success, 'code'=>200], $this->successStatus);

        }else{
            
            return response()->json(['error'=>'Login failed', 'code'=>400], 400); 
        }

    }


    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    {   
        $credentials = $request->all();
        $validator = Validator::make($request->all(), [ 
            'mobile' => 'required',
            'password' => 'required', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['dev_error'=>$validator->errors(), 'error'=>"Enter Your mobile/password", 'code'=>400], 400);            
        }

        $input = $request->all(); 
        
        if(Auth::attempt(['mobile' => request('mobile'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['user'] =  $user; 
            return response()->json(['success' => $success], $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    public function noti(){
        $this->common->auto_notification();
    }

    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 

        // DB::enableQueryLog();
        // DB::table('branches')->insert([
        //     'customer_id' => 1,
        //     'cin' => 'SQS10',
        //     'branch_name' => 'SQS - CBE',
        //     'short_name' => 'SQS',
        //     'addresss_1' => '#509, 1st floor, red rose plaza',
        //     'addresss_2' => 'DB road, RS Puram',
        //     'city_id' => 1,
        //     'state_id' => 1,
        //     'country_id' => 1,
        //     'latitude' => '11.0069882',
        //     'longitude' => '76.9485465'
        // ]);
        // print_r(DB::getQueryLog());exit;

        $user = Auth::user(); 
        return response()->json(['success' => $user], $this->successStatus); 
    } 

}
