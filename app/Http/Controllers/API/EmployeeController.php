<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Customer;
use App\Branch; 
use App\Employee; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;


class EmployeeController extends Controller 
{
    public $successStatus = 200;

    public function __construct()
    {
        $this->common = new Commonuse();
    }


    /** 
     * Employee add api
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function employee_add(Request $request){


        // $validator = Validator::make($request->all(), [ 
        //     'customer_id' => 'required',
        //     'branch_id' => 'required',
        //     'first_name' => 'required',
        //     'emp_mobile' => 'required',
        //     'emp_email' => 'required',
        // ]);

        // if ($validator->fails()) { 
        //     return response()->json(['error'=>$validator->errors(), 'code'=>400], 200);            
        // }

        $requestData = $request->all();

        $input = $requestData['values'];

        $input['customer_id'] = Auth::user()->id;

        $input['dob'] = date('Y-m-d', strtotime($input['dob']));
        $input['doj'] = date('Y-m-d', strtotime($input['doj']));

        $user_type = "E";
        $user['name'] = $input['first_name']; 
        $user['email'] = $input['emp_email']; 
        $user['mobile'] = $input['emp_mobile']; 
        $user['password'] = bcrypt("Date@123"); 
        $user['user_type'] = $user_type; 
        $user['branch_id'] = $branch_id =  $input['branch_id']; 
        $user['customer_id'] = $input['customer_id']; 

        $check_email = DB::table('employees')->where('branch_id', $user['branch_id'])
        ->where('emp_email', $user['email'])->count();
        if($check_email > 1){
            return response()->json(['error'=>"Email already exist", 'code'=>400], 200); 
            exit;
        }

        $check_mobile = DB::table('employees')->where('branch_id', $user['branch_id'])
        ->where('emp_mobile', $user['mobile'])->count();
        if($check_mobile > 1){
            return response()->json(['error'=>"Mobile already exist", 'code'=>400], 200); 
            exit;
        }
        
        $res = $this->common->add_users($user);
        if($res['status'] == 200){

            $input['user_id'] = $res['user_id'];

            $employee = Employee::create($input);

            $result['employee'] = $employee;

            $success = $result;

            $branches = DB::table('branches')->where('id', $branch_id)->first();

            $mes = "QOLPHIN Created, Your CIN is - ".$branches->cin;
            $num = $user['mobile'];

            $this->common->send_sms($mes, $num);

            return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

            
        }else{
            return response()->json(['error'=>"User not created", 'code'=>400], 200); 
        }

        

        return response()->json(['success' => $success, 'code'=>400], $this->successStatus); 

    }

    public function employee_update(Request $request){
       



        $requestData = $request->all();

        $employee_id = $requestData['id'];

        $employee = DB::table('employees')->where('id', $employee_id)->first();

        $input = $requestData['values'];

        $input['dob'] = date('Y-m-d', strtotime($input['dob']));
        $input['doj'] = date('Y-m-d', strtotime($input['doj']));

        $user['name'] = $input['first_name']; 
        $user['email'] = $input['emp_email']; 
        $user['mobile'] = $input['emp_mobile']; 
        $user['branch_id'] = $input['branch_id']; 
        

        $check_email = DB::table('employees')->where('branch_id', $user['branch_id'])
        ->where('id', '!=', $employee_id)
        ->where('emp_email', $user['email'])->count();
        if($check_email > 1){
            return response()->json(['error'=>"Email already exist", 'code'=>400], 200); 
            exit;
        }

        $check_mobile = DB::table('employees')->where('branch_id', $user['branch_id'])
        ->where('id', '!=', $employee_id)
        ->where('emp_mobile', $user['mobile'])->count();
        if($check_mobile > 1){
            return response()->json(['error'=>"Mobile already exist", 'code'=>400], 200); 
            exit;
        }

        
        $res = $this->common->update_users($user, $employee->user_id);
        if($res['status'] == 200){

            $input['user_id'] = $res['user_id'];

            $emp_update = Employee::findOrFail($employee_id);
            $emp_update->update($input);

            $result['employee'] = $employee;

            $success = $result;

            return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

            
        }else{
            return response()->json(['error'=>"User not created", 'code'=>400], 200); 
        }

        

        return response()->json(['success' => $success, 'code'=>400], $this->successStatus); 

    }


    public function employee_change_status(Request $request){
       
        $requestData = $request->all();

        $employee_id = $requestData['value_id'];
        $is_active = $requestData['is_active'];

        $res = DB::table('employees')->where('id', $employee_id)->update(['is_active'=>$is_active]);

        if($res){
            return response()->json(['success' => $res, 'code'=>200], $this->successStatus); 
        }else{
            return response()->json(['error'=>"Something went wrong", 'code'=>400], 200); 
        }
    }
    
    
    
    

    

}
