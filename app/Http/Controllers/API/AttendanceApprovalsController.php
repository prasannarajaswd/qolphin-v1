<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Customer;
use App\Branch; 
use App\Employee;
use App\Dashboard; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;


class AttendanceApprovalsController extends Controller 
{
    public $successStatus = 200;

    public function __construct()
    {
        $this->common = new Commonuse();
    }

    /** 
     * TL get approve list,
     * 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function attendance_approve_list(Request $request){
        $validator = Validator::make($request->all(), [ 
           
            // 'log_type' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>"Log type is required", 'code'=>400], 400);            
        }

        $input = $request->all();

        $user_id = Auth::user()->id;

        $employees = DB::table('employees')->where('user_id', $user_id)->first();

        $reporting_head_id = $employees->reporting_head_id;

        
        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 
        
    }

    
    

    

}
