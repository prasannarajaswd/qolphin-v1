<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\Customer;
use App\Branch; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;
use App\BranchPrevileges;
use App\AttendanceConfigs;
use App\Notifications;


class NotificationController extends Controller 
{
    public $successStatus = 200;

    public function __construct()
    {
        $this->common = new Commonuse();
    }


    /** 
     * Branch add api
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function notification(Request $request){

        if(Auth::check()){

        }else{
            return response()->json(['error'=>'Authorization Failed', 'code'=>400], 400);      
        }

        $input = $request->all(); 
            
        $user_id = Auth::user()->id;

        // [
        // {
        // title: 'Approved',
        // subTitle: 'Your attendence has been Approved',
        // hasLink: true,
        // type: 'approved',
        // isRead: false,
        // link: 'home',
        // id: 1,
        // },

        // {
        //         "id": 1,
        //         "user_id": 14,
        //         "title": "Reminder: PunchIn/PunchOut",
        //         "created_date": "2019-11-09 07:08:15",
        //         "image": null,
        //         "des": "Dont forget to Punchin/Punchout",
        //         "is_redirectable": 0,
        //         "is_active": 1,
        //         "created_at": "2019-11-09 07:08:15",
        //         "updated_at": "2019-11-09 07:08:15"
        //     },

        $notifications = Notifications::where('user_id', $user_id)
        ->where( 'is_active', 1)
        ->where( 'created_date', '>', date('Y-m-d', strtotime("-7 days")))
        ->orderby('created_date', 'desc')
        ->get();

        $notis = array();

        $result['is_notifications'] = 0;
        if(count($notifications) > 0){

            $result['is_notifications'] = 1;

            foreach ($notifications as $notification) {
                $noti['title'] = $notification->title;
                $noti['subTitle'] = $notification->des;

                if($notification->is_redirectable == 0){
                    $is_redirectable = False;
                }else{
                    $is_redirectable = True;
                }

                $noti['hasLink'] = $is_redirectable;

                //info, approved, rejected
                $noti['type'] = "info";

                if($notification->is_active == 1){
                    $is_active = False;
                }else{
                    $is_active = True;
                }

                $noti['isRead'] = $is_active;
                $noti['link'] = $notification->title;
                $noti['id'] = $notification->id;
                $noti['notification_dt'] = date('d M, Y H:i A', strtotime($notification->created_at));

                $noti['image'] = "https://qolphin-assets.s3.us-east-2.amazonaws.com/default_image.png";

                $notis[] = $noti;
            }

        }
            
        $result['notifications'] = $notis; 

        $success = $result;

        return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

    }


    //
    public function change_status(Request $request){

        if(Auth::check()){

        }else{
            return response()->json(['error'=>'Authorization Failed', 'code'=>400], 400);
        }

        $user_id = Auth::user()->id;

        $input = $request->all(); 

        if(isset($input['notification_id'])){

            $notification_id = $input['notification_id'];

            $result['status'] = Notifications::where('id', $notification_id)->update(['is_active'=>0]);

            $success = $result;

            return response()->json(['success' => $success, 'code'=>200], $this->successStatus); 

        }else{
            
            return response()->json(['error'=>'notification_id paramenters is required', 'code'=>400], 400);
        }


    }

    

}
