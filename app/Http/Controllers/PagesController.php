<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User; 
use App\Customer;
use App\Branch; 
use App\Employee;
use App\Dashboard; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use DB;
use App\Common\Commonuse;

class PagesController extends Controller
{

    public function __construct()
    {
        $this->common = new Commonuse();
    }

    public function txt1(Request $request)
    {
        return view('txt.txt1');
    }

    public function txt2(Request $request)
    {
        return view('txt.txt2');
    }


    public function privacy()
    {
        return view('privacy');
    }
    
    /**
     * Show the application branch.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function branch(Request $request)
    {
        $customer_id = Auth::user()->id;
        $branches = DB::table('branches')->where('customer_id', $customer_id)->get();
        return view('v1.branch', compact('branches'));
    }

    /**
     * Show the application edit branch.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit_branch(Request $request, $id)
    {
        $update = 1;
        $branch_id = base64_decode($id);
        $customer_id = Auth::user()->id;
        $branch = DB::table('branches')->where('id', $branch_id)->first();
        $branch_previleges = DB::table('branch_previleges')->where('branch_id', $branch_id)->where('is_active', 1)->get();
        $previleges = DB::table('previleges')->where('is_active', 1)->get();
        return view('v1.branch_add', compact('update', 'branch', 'previleges', 'branch_previleges'));
    }

    /**
     * Show the application Attendance configs.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function attendance_configs(Request $request, $id)
    {
        $update = 1;
        $branch_id = base64_decode($id);
        $customer_id = Auth::user()->id;
        $branch = DB::table('branches')->where('id', $branch_id)->first();
        $branch_previleges = DB::table('branch_previleges')->where('branch_id', $branch_id)->where('is_active', 1)->get();
        $previleges = DB::table('previleges')->where('is_active', 1)->get();
        $configs = DB::table('attendance_configs')->where('branch_id', $branch_id)->first();
        return view('v1.attendance_configs', compact('update', 'branch', 'previleges', 'branch_previleges', 'configs'));
    }

    /**
     * Show the application shifts.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function shifts(Request $request, $id)
    {
        $update = 1;
        $branch_id = base64_decode($id);
        $customer_id = Auth::user()->id;
        $branch = DB::table('branches')->where('id', $branch_id)->first();
        $branch_previleges = DB::table('branch_previleges')->where('branch_id', $branch_id)->where('is_active', 1)->get();
        $previleges = DB::table('previleges')->where('is_active', 1)->get();
        $shifts = DB::table('shifts')->where('branch_id', $branch_id)->get();

        return view('v1.shifts', compact('update', 'branch', 'previleges', 'branch_previleges', 'shifts'));
    }


    /**
     * Show the application designation.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function designation(Request $request, $id)
    {
        $update = 1;
        $branch_id = base64_decode($id);
        $customer_id = Auth::user()->id;
        $branch = DB::table('branches')->where('id', $branch_id)->first();
        $branch_previleges = DB::table('branch_previleges')->where('branch_id', $branch_id)->where('is_active', 1)->get();
        $previleges = DB::table('previleges')->where('is_active', 1)->get();

        $designations = DB::table('designations')->where('branch_id', $branch_id)->get();

        return view('v1.designation', compact('update', 'branch', 'previleges', 'branch_previleges', 'designations'));
    }

    /**
     * Show the application designation.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function holidays(Request $request, $id)
    {
        $update = 1;
        $branch_id = base64_decode($id);
        $customer_id = Auth::user()->id;
        $branch = DB::table('branches')->where('id', $branch_id)->first();
        $branch_previleges = DB::table('branch_previleges')->where('branch_id', $branch_id)->where('is_active', 1)->get();
        $previleges = DB::table('previleges')->where('is_active', 1)->get();

        $holidays = DB::table('holidays')->where('branch_id', $branch_id)->get();

        return view('v1.holidays', compact('update', 'branch', 'previleges', 'branch_previleges', 'holidays'));
    }



    /**
     * Show the login page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function authentication(Request $request)
    {
        return view('v1.login');
    }

    /**
     * Show the login page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function registration(Request $request)
    {
        return view('v1.register');
    }

    /**
     * Show the login page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function email_temp(Request $request)
    {
        return view('v1.email_templates.email_verify');
    }

    /**
     * Show the application branch add.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function branch_adding(Request $request)
    {
        $previleges = DB::table('previleges')->where('is_active', 1)->get();
        return view('v1.branch_add', compact('previleges'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function console(Request $request)
    {
        $customers = DB::table('customers')->get();

        //print_r($customers);exit;

        $customer_id = Auth::user()->id;
        $branch_ct = DB::table('branches')->where('customer_id', $customer_id)->count();
        $users_ct = DB::table('users')->where('customer_id', $customer_id)
        ->where('user_type', 'E')->count();

        return view('v1.console', compact('branch_ct', 'users_ct'));
    }

    /**
     * Show the application employee.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function employees(Request $request)
    {
        $customer_id = Auth::user()->id;

        $employees = DB::table('employees as emp')
        ->leftjoin('designations as des', 'emp.designation_id', 'des.id')
        ->leftjoin('branches as br', 'emp.branch_id', 'br.id')
        ->leftjoin('shifts as sh', 'emp.shift_id', 'sh.id')
        ->leftjoin('employees as rh', 'emp.reporting_head_id', 'rh.id')
        ->select('emp.*', 'des.designation_name', 'br.branch_name', 'sh.shift_name', 'sh.start_time', 'sh.end_time', 'rh.first_name as rh_fname', 'rh.last_name as rh_lname', 'rh.profile_pic as rh_pic')
        ->where('emp.customer_id', $customer_id)->orderby('created_at', 'desc')->paginate(10);
        
        return view('v1.employees', compact('employees'));
    }

    /**
     * Show the application employee add.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function employee_adding(Request $request)
    {
        $customer_id = Auth::user()->id;

        $branches = DB::table('branches')->where('customer_id', $customer_id)->get();

        return view('v1.employee_add', compact('branches'));
    }


    /**
     * Show the application employee_attendance
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function employee_attendance(Request $request, $id)
    {
        $employee_id = base64_decode($id);

        $employee = DB::table('employees as emp')
        ->leftjoin('designations as des', 'emp.designation_id', 'des.id')
        ->leftjoin('branches as br', 'emp.branch_id', 'br.id')
        ->leftjoin('shifts as sh', 'emp.shift_id', 'sh.id')
        ->leftjoin('employees as rh', 'emp.reporting_head_id', 'rh.id')
        ->where('emp.id', $employee_id)
        ->select('emp.*', 'des.designation_name', 'br.branch_name', 'sh.shift_name', 'sh.start_time', 'sh.end_time', 'rh.first_name as rh_fname', 'rh.last_name as rh_lname', 'rh.profile_pic as rh_pic', 'rh.emp_code as rh_emp_code')
        ->first();

       // print_r($employee);exit;

        return view('v1.employee_attendance', compact('employee', 'employee_id'));
    }

    

    public function employee_edit(Request $request, $id){
        $update = 1;
        $employee_id = base64_decode($id);
        $customer_id = Auth::user()->id;
        $branches = DB::table('branches')->where('customer_id', $customer_id)->get();
        $employee = DB::table('employees')->where('id', $employee_id)->first();
       
        return view('v1.employee_add', compact('update', 'branches', 'employee'));
    }
    
}
