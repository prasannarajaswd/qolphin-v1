<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Employee extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'branch_id', 'user_id', 'first_name', 'last_name', 'dob', 'emp_email', 'emp_mobile', 'emp_code',
        'profile_pic', 'permanent_addr', 'present_addr', 'city_id', 'state_id', 'country_id', 'reporting_head_id', 
        'is_reporting_head', 'shift_id', 'doj', 'is_active', 'designation_id',
    ];

    
}

