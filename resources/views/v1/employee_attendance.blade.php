@include('v1.header')

  @if(isset($update))
    @php $disabled = "disabled"; $bookmark_sub = "Edit";@endphp
  @else
    @php $disabled = ""; $bookmark_sub = "Add";@endphp
  @endif

  @php $breadcrumbs = "Employees / Attendances"; $active_e = "active"; @endphp

  @include('v1.menu')

    <div class="my-3 my-md-5">
      <div class="container">
        
        <div class="page-header">
          
          <!-- <div class="ml-auto" style="text-align: right;"> 
            <a href="{{ url('/branch/add') }}" class="btn btn-primary"><i class="fe fe-plus"></i> New</a>
          </div> -->
        </div>

          <div class="row">

              <div class="col-md-3">  


                  @php 
                    $active_configs = ""; $active_shifts = ""; $active_designation = "";
                    $active_holidays = ""; $active_update = "active";
                  @endphp    
                  @include('v1.employee_sidemenu')

              </div>
              <div class="col-md-9"> 
                <div class="card">
                  <div class="card-body">
                    <div id="calendar"></div>
                  </div>
                </div>
              </div>

              <!-- <div class="col-md-2">
                <div class="card">
                  <div class="card-body">
                    <div class="media">
                      <div class="media-body">
                        <h4 class="m-0">Employee details</h4>
                        

                      </div>
                    </div>
                  </div>
                </div>
              </div>   -->

          </div>
          
        
      </div>
    </div>

    <script type="text/javascript">
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            function isFloatKey(evt, obj) {

                var charCode = (evt.which) ? evt.which : event.keyCode
                var value = obj.value;
                var dotcontains = value.indexOf(".") != -1;
                if (dotcontains)
                    if (charCode == 46) return false;
                if (charCode == 46) return true;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            function getCalander(cur_month, cur_year){

              $.ajax({
                type: "get",
                url: "/my_attendances_web",
                data: {
                  "_token": "{{ csrf_token() }}",
                  "month": cur_month,
                  "year": cur_year,
                  "employee_id": "{{ $employee_id }}"
                },
                success: function(events)
                {
                  if(events.code == 200){
                    var res = events.success;

                    //console.log(res);

                    $('#calendar').fullCalendar('removeEvents');
                    $('#calendar').fullCalendar('addEventSource', res);         
                    $('#calendar').fullCalendar('rerenderEvents' );
                  }
                  
                }
              });


            }
            

            require(['jquery', 'selectize', 'fullcalendar'], function ($, selectize) {
                $(document).ready(function () {

                    var month = "";
                    var year = "";

                    var today = new Date();
                    var cur_month = today.getMonth()+1;
                    var cur_year = today.getFullYear();
                    var cur_date = today.getDate();

                    $(document).on('click', '.fc-prev-button', function (e) {
                      var tglCurrent = $('#calendar').fullCalendar('getDate');
                      var date = new Date(tglCurrent);
                      year = date.getFullYear();
                      month = date.getMonth();

                      month_ = month+1;

                      getCalander(month_, year);
                    });

                    $(document).on('click', '.fc-next-button', function (e) {
                      var tglCurrent = $('#calendar').fullCalendar('getDate');
                      var date = new Date(tglCurrent);
                      year = date.getFullYear();
                      month = date.getMonth();

                      month_ = month+1;

                      getCalander(month_, year);

                    });

                    var maxDate = cur_year+'-'+cur_month+'-'+cur_date;

                    $('#calendar').fullCalendar({
                      defaultView: 'month',
                      editable: true,
                    });

                    getCalander(cur_month, cur_year);


                    
                    $(document).on('click', '#branch_create', function (e) {

                      e.preventDefault();

                      var values = {}; var val = 0;
                      $.each($('#branch_create_form').serializeArray(), function(i, field) {

                        $("."+field.name+"_err").empty();
                        $("."+field.name).removeClass('is-invalid');

                        if($("."+field.name).val() == ""){
                          $("."+field.name).addClass('is-invalid');
                          val += 1;
                        }else{
                          $("."+field.name).addClass('state-valid');
                          
                        }
                        values[field.name] = field.value;

                      });

                      if($('#submit_value').val() == 0){
                        var url = "/branch_add";
                        data = {
                                  "_token": "{{ csrf_token() }}",
                                  "values": values
                                  };
                      }else{
                        var url = "/branch_update";
                        data = {
                                  "_token": "{{ csrf_token() }}",
                                  "values": values,
                                  "id": "@if(isset($branch)) {{$branch->id}} @endif"
                                  };
                      }

                      if(val == 0){
                        
                        console.log(values);
                        $('#pre-load').show();
                        $.ajax({
                            type: "post",
                            url: url,
                            data: data,
                            cache: false,
                            success: function(data) {
                                $('#pre-load').hide();
                                console.log(data);
                                if(data.code == 200){
                                  $(location).attr('href','/branch');
                                }
                            }
                        });


                      }else{
                        console.log("PLease fill all ");
                      }

                       
                    });

                    $('#input-tags').selectize({
                        delimiter: ',',
                        persist: false,
                        create: function (input) {
                            return {
                                value: input,
                                text: input
                            }
                        }
                    });
            
                    $('#select-beast').selectize({});
            
                    $('#select-users').selectize({
                        render: {
                            option: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    '<span class="title">' + escape(data.text) + '</span>' +
                                    '</div>';
                            },
                            item: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    escape(data.text) +
                                    '</div>';
                            }
                        }
                    });
            
                    $('#select-countries').selectize({
                        render: {
                            option: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    '<span class="title">' + escape(data.text) + '</span>' +
                                    '</div>';
                            },
                            item: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    escape(data.text) +
                                    '</div>';
                            }
                        }
                    });
                });
            });
          </script>
@include('v1.footer')