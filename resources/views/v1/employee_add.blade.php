@include('v1.header')

  @if(isset($update))
    @php $disabled = "disabled"; $bookmark_sub = "Edit";@endphp
  @else
    @php $disabled = ""; $bookmark_sub = "Add";@endphp
  @endif

  @php $breadcrumbs = "Employees / ".$bookmark_sub;  $active_e = "active"; @endphp
  
  @include('v1.menu')



    <div class="my-3 my-md-5">
      <div class="container">
        
        <div class="page-header">
          
        </div>

          @if(isset($update))
           
          <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Update Employee Details</h3>
                  </div>

                  <form action="https://httpbin.org/post" id="employee_create_form" method="post" class="card">
                    <div class="card-body">
                      @include('v1.employee_form')
                    </div>
                  </form>
                  <input type="hidden" id="submit_value" value="1">

                </div>
              </div>  

          </div>
            
          @else

            <div class="row">
              <div class="col-md-12">

                    <div class="card">

                        <div class="card-header">
                          <h3 class="card-title">Create Employee</h3>
                        </div>

                        <form action="https://httpbin.org/post" id="employee_create_form" method="post" class="card">
                          <div class="card-body">
                            @include('v1.employee_form')
                          </div>
                        </form>
                        <input type="hidden" id="submit_value" value="0">    

                    </div>
              
              </div> 
            </div>
            
          @endif
          
        
      </div>
    </div>

    <script type="text/javascript">
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            function isFloatKey(evt, obj) {

                var charCode = (evt.which) ? evt.which : event.keyCode
                var value = obj.value;
                var dotcontains = value.indexOf(".") != -1;
                if (dotcontains)
                    if (charCode == 46) return false;
                if (charCode == 46) return true;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            require(['input-mask']);

            require(['jquery', 'selectize'], function ($, selectize) {

                function get_configs(branch_id){

                  if(branch_id == null || branch_id == ""){

                    $('.branch_configs').hide();
                  }else{

                    var values = {};
                    values['branch_id'] = branch_id;

                    $('.branch_configs').hide();
                    $('#pre-load').show();
                    $.ajax({
                        type: "post",
                        url: '/get_branch_configs',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "values": values
                            },
                        cache: false,
                        success: function(data) {
                            $('#pre-load').hide();
                            console.log(data);
                            if(data.code == 200){

                              var get_data = data.success;
                              var shifts = get_data.shifts;
                              var designations = get_data.designations;
                              var rh = get_data.rh;

                              var seid = $('#shift_edit_id').val();
                              var deid = $('#designation_edit_id').val();
                              var reid = $('#reporting_head_edit_id').val();

                              var s_selected = "";
                              var d_selected = "";
                              var r_selected = "";

                              $('#shift_id').empty();
                              var output_shifts = [];
                              output_shifts.push('<option value="">Select Shift</option>');
                              $.each(shifts, function(i, j){

                                if(seid != ""){
                                  if(seid == j.id){
                                    s_selected = "selected";
                                  }else{
                                    s_selected = "";
                                  }
                                }

                                output_shifts.push('<option value="'+ j.id +'" '+s_selected+'>'+ j.shift_name+' ('+j.start_time+'-'+j.end_time+')</option>');
                              });
                              $('#shift_id').html(output_shifts.join(''));

                              $('#designation_id').empty();
                              var output_de = [];
                              output_de.push('<option value="">Select Designation</option>');
                              $.each(designations, function(k, l){

                                if(deid != ""){
                                  if(deid == l.id){
                                    d_selected = "selected";
                                  }else{
                                    d_selected = "";
                                  }
                                }

                                output_de.push('<option value="'+ l.id +'" '+d_selected+'>'+ l.designation_name +'</option>');
                              });
                              $('#designation_id').html(output_de.join(''));                                    

                              $('#reporting_head_id').empty();
                              var output_rh = [];
                              output_rh.push('<option value="">Select Reporting Head</option>');
                              $.each(rh, function(m, n){

                                var last_name = "";
                                if(n.last_name == null || n.last_name == ""){
                                  last_name = "";
                                }else{
                                  last_name = n.last_name;
                                }

                                if(reid != ""){
                                  if(reid == n.id){
                                    r_selected = "selected";
                                  }else{
                                    r_selected = "";
                                  }
                                }

                                output_rh.push('<option value="'+ n.id +'" '+r_selected+'>'+n.first_name+" "+last_name+'</option>');
                              });
                              $('#reporting_head_id').html(output_rh.join(''));

                              $('.branch_configs').show();
                            }
                        }
                    });

                    
                  }
                }

                $(document).ready(function () {

                    var bid = $('.branch_id').val();

                    if(bid != ""){
                      get_configs(bid);
                    }

                    

                    $(document).on('change', '.branch_id', function (e) {
                        e.preventDefault();
                        var branch_id = $(this).val();

                        get_configs(branch_id);
                        
                    });

                    $(document).on('click', '#employee_create', function (e) {

                      e.preventDefault();

                      var values = {}; var val = 0;
                      $.each($('#employee_create_form').serializeArray(), function(i, field) {

                        $("."+field.name+"_err").empty();
                        $("."+field.name).removeClass('is-invalid');

                        if($("."+field.name).val() == "" && field.name != 'reporting_head_id'){
                          $("."+field.name).addClass('is-invalid');
                          val += 1;
                        }else{
                          $("."+field.name).addClass('state-valid');
                          
                        }
                        values[field.name] = field.value;

                      });
                      // console.log(val);console.log('values');
                      // console.log(values);

                      if($('#submit_value').val() == 0){
                        var url = "/employee_add";
                        data = {
                                  "_token": "{{ csrf_token() }}",
                                  "values": values
                                  };
                      }else{
                        var url = "/employee_update";
                        data = {
                                  "_token": "{{ csrf_token() }}",
                                  "values": values,
                                  "id": "@if(isset($employee)) {{$employee->id}} @endif"
                                  };
                      }

                      if(val == 0){
                        
                        console.log(values);
                        $('#pre-load').show();
                        $.ajax({
                            type: "post",
                            url: url,
                            data: data,
                            cache: false,
                            success: function(data) {
                                $('#pre-load').hide();
                                console.log(data);
                                if(data.code == 200){
                                  $(location).attr('href','/employees');
                                }else{

                                }
                            }
                        });


                      }else{
                        console.log("PLease fill all ");
                      }

                       
                    });

                    $('#input-tags').selectize({
                        delimiter: ',',
                        persist: false,
                        create: function (input) {
                            return {
                                value: input,
                                text: input
                            }
                        }
                    });
            
                    $('#select-beast').selectize({});


            
                    $('#select-users').selectize({
                        render: {
                            option: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    '<span class="title">' + escape(data.text) + '</span>' +
                                    '</div>';
                            },
                            item: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    escape(data.text) +
                                    '</div>';
                            }
                        }
                    });
            
                    $('#select-countries').selectize({
                        render: {
                            option: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    '<span class="title">' + escape(data.text) + '</span>' +
                                    '</div>';
                            },
                            item: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    escape(data.text) +
                                    '</div>';
                            }
                        }
                    });
                });
            });
          </script>
@include('v1.footer')