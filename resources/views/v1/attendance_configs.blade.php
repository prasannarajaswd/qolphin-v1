@include('v1.header')
    
  @php $breadcrumbs = "Branch / Attendance COnfigs"; @endphp
    
  @include('v1.menu')

    <div class="my-3 my-md-5">
      <div class="container">
       
        <div class="page-header">
          
          <!-- <div class="ml-auto" style="text-align: right;"> 
            <a href="{{ url('/branch/add') }}" class="btn btn-primary"><i class="fe fe-plus"></i> New</a>
          </div> -->
        </div>

          <div class="row">

              <div class="col-md-3">
                    
                  @php 
                    $active_configs = "active"; $active_shifts = ""; $active_designation = "";
                    $active_holidays = ""; $active_update = "";
                  @endphp

                  @include('v1.branch_sidemenu')

              </div>
              <div class="col-md-9">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Attendance Configuration</h3>
                  </div>

                  <?php
                    $auto_approve_yes = "";$auto_approve_no = "";

                    $rh_approve_yes = "";$rh_approve_no = "";

                    $rh_level_approve_yes = "";$rh_level_approve_no = "";

                    if(isset($configs)){

                      if($configs->is_auto_approve == 1){
                        $auto_approve_yes = "checked";
                      }else if($configs->is_auto_approve == 0){
                        $auto_approve_no = "checked";
                      }

                      if($configs->is_rh_approve == 1){
                        $rh_approve_yes = "checked";
                      }else if($configs->is_rh_approve == 0){
                        $rh_approve_no = "checked";
                      }

                      if($configs->is_rh_level_approve == 1){
                        $rh_level_approve_yes = "checked";
                      }else if($configs->is_rh_level_approve == 0){
                        $rh_level_approve_no = "checked";
                      }


                    }

                  ?>

                  <form id="attendance_config_form" method="post" class="card">
                    <div class="card-body">                      

                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-6 col-lg-6">
                              <label class="form-label">If You want auto approve</label>
                            </div>
                            <div class="col-md-6 col-lg-6">
                              <div class="selectgroup w-100">
                                <label class="selectgroup-item">
                                  <input type="radio" name="is_auto_approve" value="1" class="selectgroup-input" {{$auto_approve_yes}}> <span class="selectgroup-button">Yes</span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="is_auto_approve" value="0" class="selectgroup-input" {{$auto_approve_no}}> <span class="selectgroup-button">No</span>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-6 col-lg-6">
                              <label class="form-label">If You want Reporting head approve</label>
                            </div>
                            <div class="col-md-6 col-lg-6">
                              <div class="selectgroup w-100">
                                <label class="selectgroup-item">
                                  <input type="radio" name="is_rh_approve" value="1" class="selectgroup-input" {{$rh_approve_yes}}> <span class="selectgroup-button">Yes</span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="is_rh_approve" value="0" class="selectgroup-input" {{$rh_approve_no}}> <span class="selectgroup-button">No</span>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="form-group" style="display: none;">
                          <div class="row">
                            <div class="col-md-6 col-lg-6">
                              <label class="form-label">If You want Reporting head level approve</label>
                            </div>
                            <div class="col-md-6 col-lg-6">
                              <div class="selectgroup w-100">
                                <label class="selectgroup-item">
                                  <input type="radio" name="is_rh_level_approve" value="1" class="selectgroup-input"> <span class="selectgroup-button">Yes</span>
                                </label>
                                <label class="selectgroup-item">
                                  <input type="radio" name="is_rh_level_approve" value="0" class="selectgroup-input"> <span class="selectgroup-button">No</span>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="form-group level_rh" style="display: none;">
                            <div class="row">
                              <div class="col-md-6 col-lg-6">
                                <label class="form-label">Level size</label>
                              </div>
                              <div class="col-md-6 col-lg-6">
                                <input  type="text" class="form-control pincode" name="level_size" placeholder="3" onkeypress='return isNumberKey(event)'>
                                <div class="invalid-feedback pincode_err">Invalid feedback</div>
                              </div>
                            </div>
                        </div>

                    </div>

                    <div class="error-msg align-center">
                        <p id="error_msg" style="color:red;"></p>
                        <p id="succ_msg" style="color:green;"></p>
                    </div>

                    <div class="d-flex">
                        <button type="submit" id="config_create" class="btn btn-primary ml-auto">Update Configuration</button>
                    </div> 

                  </form>

                </div>
              </div>  

          </div>
          
        
      </div>
    </div>

    <script type="text/javascript">
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            
            require(['jquery', 'selectize'], function ($, selectize) {
                $(document).ready(function () {

                    
                    $(document).on('click', '#config_create', function (e) {

                      e.preventDefault();

                      $("#error_msg").empty();

                      auto_approve = $("input[name='is_auto_approve']:checked").val();
                      rh_approve = $('input[name="is_rh_approve"]:checked').val();
                      //rh_level_approve = $('input[name="is_rh_level_approve"]:checked').val();

                      console.log(auto_approve+'='+rh_approve);

                      if(auto_approve == null && rh_approve == null){
                        $("#error_msg").html('Please select atleast one!');
                        return false;
                      }else if(auto_approve == 0 && rh_approve == 0){
                        $("#error_msg").html('Please select one yes!');
                      }else{

                          var values = {};
                          if(auto_approve == 1 || auto_approve == 0){
                            values['is_auto_approve'] = auto_approve;
                          }

                          if(rh_approve == 1 || rh_approve == 0){
                            values['is_rh_approve'] = rh_approve;
                          }

                          $('#pre-load').show();
                          $.ajax({
                              type: "post",
                              url: '/attendance_config',
                              data: {
                                "_token": "{{ csrf_token() }}",
                                "values": values,
                                "id": "@if(isset($branch)) {{$branch->id}} @endif"
                              },
                              cache: false,
                              success: function(data) {
                                  $('#pre-load').hide();
                                  if(data.code == 200){
                                    $("#succ_msg").html('Sucessfully update');
                                    $(location).attr('href','/branch/configs/{{base64_encode($branch->id)}}');
                                  }
                              }
                          });

                      }

                      // if(val == 0){
                        
                      //   console.log(values);
                      


                      // }else{
                      //   console.log("PLease fill all ");
                      // }

                    });

                    $(document).on('click', '.selectgroup-input', function () {

                      name = $(this).attr('name');
                      value = this.value;

                      $('input:radio[name="is_auto_approve"]').prop('disabled',false);
                      $('input:radio[name="is_rh_approve"]').prop('disabled',false);
                      //$('input:radio[name="is_rh_level_approve"]').prop('disabled',false);

                      $('input:radio[name="is_auto_approve"]').removeAttr('checked');
                      $('input:radio[name="is_rh_approve"]').removeAttr('checked');
                      //$('input:radio[name="is_rh_level_approve"]').removeAttr('checked');

                      if(name == "is_auto_approve" && value == 1){

                        $('input:radio[name="is_auto_approve"][value="1"]').attr('checked',true);
                        $('input:radio[name="is_rh_approve"][value="0"]').attr('checked',true);
                        //$('input:radio[name="is_rh_level_approve"][value="0"]').attr('checked',true);

                        $('input:radio[name="is_rh_approve"]').prop('disabled',true);
                        //$('input:radio[name="is_rh_level_approve"]').prop('disabled',true);
                      }else if(name == "is_auto_approve" && value == 0){

                        $('input:radio[name="is_auto_approve"][value="0"]').attr('checked',true);
                        
                      }else if(name == "is_rh_approve" && value == 1){

                        $('input:radio[name="is_auto_approve"][value="0"]').attr('checked',true);
                        $('input:radio[name="is_rh_approve"][value="1"]').attr('checked',true);
                        //$('input:radio[name="is_rh_level_approve"][value="0"]').attr('checked',true);

                        $('input:radio[name="is_auto_approve"]').prop('disabled',true);
                        //$('input:radio[name="is_rh_level_approve"]').prop('disabled',true);
                        
                      }else if(name == "is_rh_approve" && value == 0){

                        $('input:radio[name="is_rh_approve"][value="0"]').attr('checked',true);                        
                      }
                      // else if(name == "is_rh_level_approve" && value == 1){

                      //   $('input:radio[name="is_auto_approve"][value="0"]').attr('checked',true);
                      //   $('input:radio[name="is_rh_approve"][value="0"]').attr('checked',true);
                      //   $('input:radio[name="is_rh_level_approve"][value="1"]').attr('checked',true);

                      //   $('input:radio[name="is_auto_approve"]').prop('disabled',true);
                      //   $('input:radio[name="is_rh_approve"]').prop('disabled',true);
                        
                      // }else if(name == "is_rh_level_approve" && value == 0){

                      //   $('input:radio[name="is_rh_level_approve"][value="0"]').attr('checked',true);                        
                      // }
                                             
                    });

                    $('#input-tags').selectize({
                        delimiter: ',',
                        persist: false,
                        create: function (input) {
                            return {
                                value: input,
                                text: input
                            }
                        }
                    });
            
                    $('#select-beast').selectize({});
            
                    $('#select-users').selectize({
                        render: {
                            option: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    '<span class="title">' + escape(data.text) + '</span>' +
                                    '</div>';
                            },
                            item: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    escape(data.text) +
                                    '</div>';
                            }
                        }
                    });
            
                    $('#select-countries').selectize({
                        render: {
                            option: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    '<span class="title">' + escape(data.text) + '</span>' +
                                    '</div>';
                            },
                            item: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    escape(data.text) +
                                    '</div>';
                            }
                        }
                    });
                });
            });
          </script>
@include('v1.footer')