@include('v1.header')
    
  @php $breadcrumbs = "Branches"; $active_b = "active"; @endphp 

  @include('v1.menu')
      <div class="my-3 my-md-5">
        <div class="container">
          <div class="page-header">
            <div class="ml-auto" style="text-align: right;"> 
              <a href="{{ url('/branch/add') }}" class="btn btn-primary"><i class="fe fe-plus"></i> New</a>
            </div>
          </div>
          <div class="row row-cards">

            @if(isset($branches))

            @foreach($branches as $k =>  $branch)

            <div class="col-lg-4">
              <div class="card" style="background-color: rgba(220, 222, 227, 0.1);">
                
                <div class="card-body d-flex flex-column">
                  <h4><a href="{{url('branch/edit/'.base64_encode($branch->id))}}">{{$branch->branch_name}} ( {{$branch->short_name}} )</a></h4>
                  <div class="text-muted">{{$branch->addresss_1}}, {{$branch->addresss_2}}</div>
                  <div class="text-muted">{{$branch->city_name}}, {{$branch->state_name}},</div>
                  <div class="text-muted">{{$branch->country_name}} - {{$branch->pincode}}.</div>
                  <div class="d-flex align-items-center pt-5 mt-auto">
                    <!-- <div class="avatar avatar-md mr-3" style="background-image: url({{asset('assets/demo/brand/tabler.svg')}})"></div-->
                    <div> <a href="{{url('branch/edit/'.base64_encode($branch->id))}}" class="text-default">CIN: {{$branch->cin}}</a>
                      <small class="d-block text-muted">{{date('d M, Y', strtotime($branch->created_at))}}</small>
                    </div>
                    <div class="ml-auto text-muted"> <a href="{{url('branch/edit/'.base64_encode($branch->id))}}" class="icon d-none d-md-inline-block ml-3"><i class="fe fe-chevrons-right"></i></a>
                    </div>
                  </div>
                </div>
                <div class="card-status card-status-bottom bg-purple"></div>
              </div>
            </div>
            @endforeach

            @endif
            

            

          </div>
        </div>
      </div>

    
@include('v1.footer')