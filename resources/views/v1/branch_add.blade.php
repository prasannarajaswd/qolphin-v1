@include('v1.header')

  @if(isset($update))
    @php $disabled = "disabled"; $bookmark_sub = "Edit";@endphp
  @else
    @php $disabled = ""; $bookmark_sub = "Add";@endphp
  @endif

  @php $breadcrumbs = "Branches / ".$bookmark_sub; $active_b = "active"; @endphp

  @include('v1.menu')

    <div class="my-3 my-md-5">
      <div class="container">
        
        <div class="page-header">
          
          <!-- <div class="ml-auto" style="text-align: right;"> 
            <a href="{{ url('/branch/add') }}" class="btn btn-primary"><i class="fe fe-plus"></i> New</a>
          </div> -->
        </div>

          @if(isset($update))
           
          <div class="row">

              <div class="col-md-3">  

                  @php 
                    $active_configs = ""; $active_shifts = ""; $active_designation = "";
                    $active_holidays = ""; $active_update = "active";
                  @endphp    
                  @include('v1.branch_sidemenu')

              </div>
              <div class="col-md-9">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Update branch Details</h3>
                  </div>

                  <form action="https://httpbin.org/post" id="branch_create_form" method="post" class="card">
                    <div class="card-body">
                      @include('v1.branch_form')
                    </div>
                  </form>
                  <input type="hidden" id="submit_value" value="1">

                </div>
              </div>  

          </div>
            
          @else

            <div class="row">
              <div class="col-md-12">

                    <div class="card">

                        <div class="card-header">
                          <h3 class="card-title">Create branch</h3>
                        </div>

                        <form action="https://httpbin.org/post" id="branch_create_form" method="post" class="card">
                          <div class="card-body">
                            @include('v1.branch_form')
                          </div>
                        </form>
                        <input type="hidden" id="submit_value" value="0">    

                    </div>
              
              </div> 
            </div>
            
          @endif
          
        
      </div>
    </div>

    <script type="text/javascript">
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            function isFloatKey(evt, obj) {

                var charCode = (evt.which) ? evt.which : event.keyCode
                var value = obj.value;
                var dotcontains = value.indexOf(".") != -1;
                if (dotcontains)
                    if (charCode == 46) return false;
                if (charCode == 46) return true;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
            

            require(['jquery', 'selectize'], function ($, selectize) {
                $(document).ready(function () {

                    $(document).on('click', '#branch_create', function (e) {

                      e.preventDefault();

                      var values = {}; var val = 0;
                      $.each($('#branch_create_form').serializeArray(), function(i, field) {

                        $("."+field.name+"_err").empty();
                        $("."+field.name).removeClass('is-invalid');

                        if($("."+field.name).val() == ""){
                          $("."+field.name).addClass('is-invalid');
                          val += 1;
                        }else{
                          $("."+field.name).addClass('state-valid');
                          
                        }
                        values[field.name] = field.value;

                      });

                      if($('#submit_value').val() == 0){
                        var url = "/branch_add";
                        data = {
                                  "_token": "{{ csrf_token() }}",
                                  "values": values
                                  };
                      }else{
                        var url = "/branch_update";
                        data = {
                                  "_token": "{{ csrf_token() }}",
                                  "values": values,
                                  "id": "@if(isset($branch)) {{$branch->id}} @endif"
                                  };
                      }

                      if(val == 0){
                        
                        console.log(values);
                        $('#pre-load').show();
                        $.ajax({
                            type: "post",
                            url: url,
                            data: data,
                            cache: false,
                            success: function(data) {
                                $('#pre-load').hide();
                                console.log(data);
                                if(data.code == 200){
                                  $(location).attr('href','/branch');
                                }
                            }
                        });


                      }else{
                        console.log("PLease fill all ");
                      }

                       
                    });

                    $('#input-tags').selectize({
                        delimiter: ',',
                        persist: false,
                        create: function (input) {
                            return {
                                value: input,
                                text: input
                            }
                        }
                    });
            
                    $('#select-beast').selectize({});
            
                    $('#select-users').selectize({
                        render: {
                            option: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    '<span class="title">' + escape(data.text) + '</span>' +
                                    '</div>';
                            },
                            item: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    escape(data.text) +
                                    '</div>';
                            }
                        }
                    });
            
                    $('#select-countries').selectize({
                        render: {
                            option: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    '<span class="title">' + escape(data.text) + '</span>' +
                                    '</div>';
                            },
                            item: function (data, escape) {
                                return '<div>' +
                                    '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                                    escape(data.text) +
                                    '</div>';
                            }
                        }
                    });
                });
            });
          </script>
@include('v1.footer')