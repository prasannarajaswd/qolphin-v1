@include('v1.header')

  @php $breadcrumbs = "Branch / Shifts"; @endphp
  
  @include('v1.menu')

    <div class="my-3 my-md-5">
      <div class="container">
       
        <div class="page-header">
          <h1 class="page-title">
                    Branches <i class="fe fe-chevron-right"></i> Shifts
          </h1>
          <!-- <div class="ml-auto" style="text-align: right;"> 
            <a href="{{ url('/branch/add') }}" class="btn btn-primary"><i class="fe fe-plus"></i> New</a>
          </div> -->
        </div>

          <div class="row">

              <div class="col-md-3">
                    
                  @php 
                    $active_configs = ""; $active_shifts = "active"; $active_designation = "";
                    $active_holidays = ""; $active_update = "";
                  @endphp

                  @include('v1.branch_sidemenu')

              </div>
              <div class="col-md-9">
                <!--start--->
                  
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Shifts</h3>
                    <button type="submit" class="btn btn-primary ml-auto add_btn" data-toggle="modal" data-target="#AddShifts">Add</button>
                  </div>
                  <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap datatable">
                      <thead>
                        <tr>
                          <th class="w-1">No.</th>
                          <th>Shift name</th>
                          <th>Start time</th>
                          <th>End time</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        
                        @if(isset($shifts))

                          @foreach($shifts as $k => $shift)
                          <tr>

                            @php $k = $k+1; @endphp
                            <td>{{$k}}</td>
                            <td id="td_name_{{$shift->id}}">{{$shift->shift_name}}</td>
                            <td id="td_start_{{$shift->id}}">{{$shift->start_time}}</td>
                            <td id="td_end_{{$shift->id}}">{{$shift->end_time}}</td>
                            <td class="text-right">
                              <a href="javascript:void(0)" id="td_edit_{{$shift->id}}" class="btn btn-secondary btn-sm edit_btn" data-toggle="modal" data-target="#AddShifts">Edit</a>
                              <a href="javascript:void(0)" class="btn btn-secondary btn-sm">Delete</a>
                            </td>
                         

                          </tr>
                          @endforeach


                        @endif
                        
                      </tbody>
                    </table>
                    <script>
                      require(['datatables', 'jquery'], function(datatable, $) {
                            $('.datatable').DataTable({
                              "bPaginate": false,
                              "pageLength": 5
                            });
                          });
                    </script>
                  </div>
                </div>

                <!--end--->

              </div>  

          </div>
          
        
      </div>
    </div>

    <div id="AddShifts" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title shift-title">Add Designation</h4>
            <button type="submit" class="btn btn-primary ml-auto shift-btn">Save</button>
          </div>
          <div class="modal-body">

              <div class="error-msg align-center">
                  <p id="error_msg" style="color:red;"></p>
                  <p id="succ_msg" style="color:green;"></p>
              </div>
              
              <input type="text" name="id_value" id="id_value" hidden>

              <div class="form-group">
                <label class="form-label">Shift Name</label>
                <input type="text" class="form-control shift_name" name="shift_name" placeholder="Customer support executive">
              </div>

              <div class="form-group">
                <label class="form-label">Start Time</label>
                <input type="text" class="form-control start_time" name="start_time" data-mask="00:00" data-mask-clearifnotmatch="true" placeholder="00:00" autocomplete="off" maxlength="6">
              </div>
              <div class="form-group">
                <label class="form-label">End Time</label>
                <input type="text" class="form-control end_time" name="end_time" data-mask="00:00" data-mask-clearifnotmatch="true" placeholder="00:00" autocomplete="off" maxlength="6">
              </div>
              

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>


    <script type="text/javascript">
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            require(['input-mask']);
            
            require(['jquery', 'selectize'], function ($, selectize) {
                $(document).ready(function () {

                    
                    $(document).on('click', '.add_btn', function (e) {

                      e.preventDefault();

                      $('.shift-title').empty();
                      $('.shift-btn').empty();
                      $('.shift-title').html('Add Shift');
                      $('.shift-btn').html('Save');

                      $('#id_value').empty();
                      $('.shift_name').empty();
                      $('.start_time').empty();
                      $('.end_time').empty();
                      


                    });

                    $(document).on('click', '.edit_btn', function (e) {

                      e.preventDefault();

                      $('.shift-title').empty();
                      $('.shift-btn').empty();
                      $('.shift-title').html('Edit Shifts');
                      $('.shift-btn').html('Update');

                      $('#id_value').empty();
                      $('.shift_name').empty();
                      $('.start_time').empty();
                      $('.end_time').empty();

                      var get_id = $(this).attr('id');
                      var explode = get_id.split('_');
                      var value_id = explode[2];

                      var dname = $('#td_name_'+value_id).html();
                      $('.shift_name').val(dname);

                      var stime = $('#td_start_'+value_id).html();
                      $('.start_time').val(dname);

                      var etime = $('#td_end_'+value_id).html();
                      $('.end_time').val(etime);
                      
                      $('#id_value').val(value_id);
                      

                    });

                    $(document).on('click', '.shift-btn', function (e) {

                      e.preventDefault();

                      $("#error_msg").empty(); $("#succ_msg").empty();

                      var shift_name = $('.shift_name').val();
                      var start_time = $('.start_time').val();
                      var end_time = $('.end_time').val();

                      var values = {};
                      values['shift_name'] = shift_name;
                      values['start_time'] = start_time;
                      values['end_time'] = end_time;

                      var value_id = $('#id_value').val();

                      console.log('value_id'+value_id);

                      if(value_id == "" || value_id == null || value_id == 0){
                        var url =  '/shift_add';
                        var data = {
                              "_token": "{{ csrf_token() }}",
                              "values": values,
                              "branch_id": "@if(isset($branch)) {{$branch->id}} @endif"
                            };
                      }else{
                        var url =  '/shift_update';
                        var data = {
                              "_token": "{{ csrf_token() }}",
                              "values": values,
                              "branch_id": "@if(isset($branch)) {{$branch->id}} @endif",
                              "value_id": value_id
                            };
                      }

                      if(shift_name == null || shift_name == ""){
                        $("#error_msg").html('Please enter shift name');
                        return false;
                      }

                      if(start_time == null || start_time == ""){
                        $("#error_msg").html('Please enter start time');
                        return false;
                      }

                      if(end_time == null || end_time == ""){
                        $("#error_msg").html('Please enter end time');
                        return false;
                      }

                      $('#pre-load').show();
                      $.ajax({
                          type: "post",
                          url: url,
                          data: data,
                          cache: false,
                          success: function(data) {
                              $('#pre-load').hide();
                              if(data.code == 200){
                                $("#succ_msg").html('Shift updated successfully');
                                $(location).attr('href','/branch/shifts/{{base64_encode($branch->id)}}');
                              }
                          }
                      });
                                             
                    });

                    
                });
            });
          </script>
@include('v1.footer')