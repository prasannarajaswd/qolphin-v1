

                <!--  -->

                <div class="row"><!--Start row-->

                  <div class="col-md-6 col-lg-4">
                    <div class="form-group">
                      <label class="form-label">CIN <span>(This CIN is access your application login)</span> </label>
                      
                      <input type="text" style="text-transform: uppercase;" class="form-control cin" name="cin" maxlength="5" placeholder="ABC01" value="@if(isset($branch)) {{$branch->cin}} @endif" {{$disabled}}>
                      <div class="invalid-feedback cin_err">Invalid feedback</div>
                    </div>
                    
                    <div class="form-group">
                        <label class="form-label">Address line 1</label>
                        <input type="text" class="form-control addresss_1" name="addresss_1" placeholder="#509, ABC Street," value="@if(isset($branch)) {{$branch->addresss_1}} @endif">
                        <div class="invalid-feedback addresss_1_1_err">Invalid feedback</div>
                    </div>

                    <div class="form-group">
                        <label class="form-label">State</label>
                        <input type="text" class="form-control state_name" name="state_name" placeholder="Tamilnadu" value="@if(isset($branch)) {{$branch->state_name}} @endif">
                        <div class="invalid-feedback state_name_err">Invalid feedback</div>
                    </div>

                    <div class="form-group">
                        <label class="form-label">Latitude</label>
                        <input type="text" class="form-control latitude" name="latitude" placeholder="11.5967458" value="@if(isset($branch)) {{$branch->latitude}} @endif" onkeypress='return isFloatKey(event,this)'>
                        <div class="invalid-feedback latitude_err">Invalid feedback</div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                  <div class="form-group">
                      <label class="form-label">Branch name</label>
                      <input type="text" class="form-control branch_name" name="branch_name" placeholder="Saturnq solution pvt ltd" value="@if(isset($branch)) {{$branch->branch_name}} @endif">
                      <div class="invalid-feedback branch_name_err">Invalid feedback</div>
                  </div>

                  <div class="form-group">
                      <label class="form-label">Address line 2</label>
                      <input type="text" class="form-control addresss_2" name="addresss_2" placeholder="XY ABCD, AB" value="@if(isset($branch)) {{$branch->addresss_2}} @endif">
                      <div class="invalid-feedback addresss_2_err">Invalid feedback</div>
                  </div>

                  <div class="form-group">
                      <label class="form-label">Country</label>
                      <input type="text" class="form-control country_name" name="country_name" placeholder="India" value="@if(isset($branch)) {{$branch->country_name}} @endif">
                      <div class="invalid-feedback country_name_err">Invalid feedback</div>
                  </div>

                  <div class="form-group">
                      <label class="form-label">Longitude</label>
                      <input type="text" class="form-control longitude" name="longitude" placeholder="77.1167458" value="@if(isset($branch)) {{$branch->longitude}} @endif" onkeypress='return isFloatKey(event,this)' >
                      <div class="invalid-feedback longitude_err">Invalid feedback</div>
                  </div>
                </div>

                <div class="col-md-6 col-lg-4">
                  <div class="form-group">
                    <label class="form-label">Branch Short name</label>
                    <input type="text" class="form-control short_name" name="short_name" placeholder="SQS-CBE" value="@if(isset($branch)) {{$branch->short_name}} @endif">
                    <div class="invalid-feedback short_name_err">Invalid feedback</div>
                  </div>

                  <div class="form-group">
                      <label class="form-label">City</label>
                      <input type="text" class="form-control city_name" name="city_name" placeholder="Coimbatore" value="@if(isset($branch)) {{$branch->city_name}} @endif">
                      <div class="invalid-feedback city_name_err">Invalid feedback</div>
                  </div>

                  <div class="form-group">
                      <label class="form-label">Pincode</label>
                      <input type="text" class="form-control pincode" name="pincode" placeholder="600001" value="@if(isset($branch)) {{$branch->pincode}} @endif" onkeypress='return isNumberKey(event)'>
                      <div class="invalid-feedback pincode_err">Invalid feedback</div>
                  </div>
                </div>

              </div><!--End row-->

              

              <div class="row"><!--Start row-->
                @foreach($previleges as $previlege)

                @php $ychecked = "checked"; $nchecked = ""; @endphp

                @if(isset($branch_previleges))
                  <?php
                  $branch_previleges = DB::table('branch_previleges')->where('branch_id', $branch->id)
                  ->where('previlege_id', $previlege->id)->where('is_active', 1)->first();
                  ?>

                  @php $ychecked = ""; $nchecked = ""; @endphp
                  @if(isset($branch_previleges))
                    @if($branch_previleges->answer == 1)
                      @php $ychecked = "checked"; $nchecked = ""; @endphp
                    @else
                      @php $ychecked = ""; $nchecked = "checked"; @endphp
                    @endif
                  @endif
                  
                @endif
                

                <div class="col-md-4 col-lg-4">
                  <div class="form-group">
                    <label class="form-label">{{$previlege->previlege}}</label>
                    <div class="selectgroup w-100">
                      <label class="selectgroup-item">
                        <input type="radio" name="previlege_id_{{$previlege->id}}" value="1" class="selectgroup-input" {{$ychecked}}> <span class="selectgroup-button">Yes</span>
                      </label>
                      <label class="selectgroup-item">
                        <input type="radio" name="previlege_id_{{$previlege->id}}" value="0" class="selectgroup-input" {{$nchecked}}> <span class="selectgroup-button">No</span>
                      </label>
                    </div>
                  </div>
                </div> 

                @endforeach
              </div><!--End row-->

              <div class="d-flex">
                  @if(isset($update))
                    @php $btn_name = "Update"; $submit_value = 1;  @endphp
                  @else
                    @php $btn_name = "Create"; $submit_value = 0; @endphp
                  @endif
                  <button type="submit" id="branch_create" class="btn btn-primary ml-auto">{{$btn_name}}</button>
              </div>   

              <!--  -->
                   

            