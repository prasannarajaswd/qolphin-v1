<div class="card">
  <div class="card-body">
    <div class="media">
      <span class="avatar avatar-xxl mr-5" style="background-image: url({{$employee->profile_pic}})"></span>
      <div class="media-body">
        <h4 class="m-0">{{$employee->first_name}} {{$employee->last_name}}</h4>
        <p class="text-muted mb-0">{{$employee->designation_name}}</p>
        <ul class="social-links list-inline mb-0 mt-2">
          
          <li class="list-inline-item">
            <a href="javascript:void(0)" title="" data-toggle="tooltip" data-original-title="{{$employee->emp_mobile}}"><i class="fa fa-phone"></i></a>
          </li>
          <li class="list-inline-item">
            <a href="javascript:void(0)" title="" data-toggle="tooltip" data-original-title="{{$employee->emp_email}}"><i class="fa fa-envelope-open-o"></i></a>
          </li>
        </ul>
      </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-6">
        <div class="h6">Employee Code</div>
        <p>{{$employee->emp_code}}</p>
      </div>
      <div class="col-6">
        <div class="h6">Branch name</div>
        <p>{{$employee->branch_name}}</p>
      </div>
      <div class="col-6">
        <div class="h6">Shift</div>
        <p>{{$employee->shift_name}} ({{$employee->start_time}} - {{$employee->end_time}})</p>
      </div>
      <div class="col-6">
        <div class="h6">Date of birth</div>
        <p>{{$employee->dob}}</p>
      </div>
      <div class="col-6">
        <div class="h6">Date of joining</div>
        <p>{{$employee->doj}}</p>
      </div>
    </div>

    <div class="h6">Present address</div>
    <p>{{$employee->present_addr}}</p>

    <div class="h6">Permanent address</div>
    <p>{{$employee->permanent_addr}}</p>

    <br><br>
    <div class="h4">Reporting Head</div>
    <div class="media">
      <span class="avatar avatar-xxl mr-5" style="background-image: url({{$employee->rh_pic}}); width: 1em; height: 1em;"></span>
      <div class="media-body">
        <h6 class="m-0" style="margin: 6px !important;">{{$employee->rh_fname}} {{$employee->rh_lname}}</h6>
      </div>
    </div>
    

  </div>
</div>