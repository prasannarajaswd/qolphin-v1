@include('v1.header')
  
  <body class="">
    <div class="page" style="background-color: #fff;">
      <div class="page-single">
        <div class="container">
          <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6">
                <img src="{{asset('assets/images/logo/logo_1.png')}}" class="h-6" alt=""  style="background-color: #fff; height: 8rem !important;" >
              </div>
              <form class="card" action="" method="post" id="login_form">
                <div class="card-body p-6">
                  <div class="card-title">Login to your account</div>
                  <div class="error-msg align-center">
                      <p id="error_msg" style="color:red;"></p>
                      <!-- <p id="succ_msg" style="color:green;"></p> -->
                  </div>
                  <div class="form-group">
                    <label class="form-label">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label class="form-label">
                      Password
                      <a href="./forgot-password.html" class="float-right small">I forgot password</a>
                    </label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" />
                      <span class="custom-control-label">Remember me</span>
                    </label>
                  </div>
                  <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block login_btn">Sign in</button>
                  </div>
                </div>
              </form>
              <!-- <div class="text-center text-muted">
                Don't have account yet? <a href="./register.html">Sign up</a>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>

  <script type="text/javascript">

    require(['jquery'], function ($, selectize) {
      $(document).ready(function () {

            function ValidateEmail(mail) 
            {
             if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
              {
                return (true)
              }
                //alert("You have entered an invalid email address!")
                return (false)
            }

            $(document).on('click', '.login_btn', function (event) {
              event.preventDefault();
              
              $('#error_msg').empty();$('#succ_msg').empty();

              var values = {};
              $.each($('#login_form').serializeArray(), function(i, field) {
                  values[field.name] = field.value;
              });

              if(values.email != "" && values.password != ""){

                  if(ValidateEmail(values.email)){

                    $.ajax({
                      type: "post",
                      url: "/login",
                      data: {
                              "_token": "{{ csrf_token() }}",
                              "values": values
                            },
                      cache: false,
                      success: function(data){
                          console.log(data);
                          if(data.code == 200){
                              $('#succ_msg').html(data.response);
                              $(location).attr('href','/console');
                          }else{
                              $('#error_msg').html(data.response);
                          }

                      }
                    });

                  }else{
                    $('#error_msg').html("You have entered an invalid email address!");
                  }

                  
              }else{
                  $('#error_msg').html("Please fill email and password");
              }   

              
            });

      });
    });

    
  </script>

    
@include('v1.footer')