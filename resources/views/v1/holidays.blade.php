@include('v1.header')

  @php $breadcrumbs = "Branch / Holidays"; @endphp
  @include('v1.menu')

    <div class="my-3 my-md-5">
      <div class="container">
       
        <div class="page-header">
          <h1 class="page-title">
                    Branches <i class="fe fe-chevron-right"></i> Shifts
          </h1>
          <!-- <div class="ml-auto" style="text-align: right;"> 
            <a href="{{ url('/branch/add') }}" class="btn btn-primary"><i class="fe fe-plus"></i> New</a>
          </div> -->
        </div>

          <div class="row">

              <div class="col-md-3">
                    
                  @php 
                    $active_configs = ""; $active_shifts = ""; $active_designation = "";
                    $active_holidays = "active"; $active_update = "";
                  @endphp

                  @include('v1.branch_sidemenu')

              </div>
              <div class="col-md-9">
                <!--start--->
                  
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Holidays</h3>
                    <button type="submit" class="btn btn-primary ml-auto add_btn" data-toggle="modal" data-target="#AddHoliday">Add</button>
                  </div>
                  <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap datatable">
                      <thead>
                        <tr>
                          <th class="w-1">No.</th>
                          <th>Holiday Date</th>
                          <th>Holiday name</th>
                          <th hidden></th>
                          <th>Status</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        
                        @if(isset($holidays))

                          @foreach($holidays as $k => $holiday)
                          <tr>

                            @php $k = $k+1; @endphp
                            <td>{{$k}}</td>
                            <td id="td_date_{{$holiday->id}}">{{date('d-m-Y', strtotime($holiday->holiday_date))}}</td>
                            <td id="td_name_{{$holiday->id}}">{{$holiday->holiday_name}}</td>
                            <td hidden id="td_status_{{$holiday->id}}">{{$holiday->is_active}}</td>
                            <td>
                            @if($holiday->is_active == 1)
                            Active
                            @else
                            Inactive
                            @endif
                            </td>
                            <td class="text-right">
                              <a href="javascript:void(0)" id="td_edit_{{$holiday->id}}" class="btn btn-secondary btn-sm edit_btn" data-toggle="modal" data-target="#AddHoliday">Edit</a>
                              <a href="javascript:void(0)" class="btn btn-secondary btn-sm">Delete</a>
                            </td>
                         

                          </tr>
                          @endforeach


                        @endif
                        
                      </tbody>
                    </table>
                    <script>
                      require(['datatables', 'jquery'], function(datatable, $) {
                            $('.datatable').DataTable({
                              //"bPaginate": false,
                              "pageLength": 5
                            });
                          });
                    </script>
                  </div>
                </div>

                <!--end--->

              </div>  

          </div>
          
        
      </div>
    </div>

    <div id="AddHoliday" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title holiday-title">Add Holiday</h4>
            <button type="submit" class="btn btn-primary ml-auto holiday-btn">Save</button>
          </div>
          <div class="modal-body">

              <div class="error-msg align-center">
                  <p id="error_msg" style="color:red;"></p>
                  <p id="succ_msg" style="color:green;"></p>
              </div>
              
              <input type="text" name="id_value" id="id_value" hidden>

              <div class="form-group">
                <label class="form-label">Holiday Date</label>
                <input type="text" name="holiday_date" class="form-control holiday_date" data-mask="00-00-0000" data-mask-clearifnotmatch="true" placeholder="DD-MM-YYYY" autocomplete="off" maxlength="10">
              </div>
              <div class="form-group">
                <label class="form-label">Holiday Name</label>
                <input type="text" class="form-control holiday_name" name="holiday_name" placeholder="New year">
              </div>
              <div class="form-group">
                <label class="form-label">Status</label>
                <div class="selectgroup w-100">
                  <label class="selectgroup-item">
                    <input type="radio" name="is_active" value="1" class="selectgroup-input" checked>
                    <span class="selectgroup-button">Active</span>
                  </label>
                  <label class="selectgroup-item">
                    <input type="radio" name="is_active" value="0" class="selectgroup-input">
                    <span class="selectgroup-button">Inactive</span>
                  </label>
                </div>
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>


    <script type="text/javascript">

            require(['input-mask']);

            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            
            require(['jquery', 'selectize'], function ($, selectize) {
                $(document).ready(function () {

                    
                    $(document).on('click', '.add_btn', function (e) {

                      e.preventDefault();

                      $('.holiday-title').empty();
                      $('.holiday-btn').empty();
                      $('.holiday-title').html('Add Holiday');
                      $('.holiday-btn').html('Save');

                      $('#id_value').empty();
                      $('.holiday_date').empty();
                      $('.holiday_name').empty();
                      $('input:radio[name="is_active"]').removeAttr('checked');
                      $('input:radio[name="is_active"][value="1"]').attr('checked',true);


                    });

                    $(document).on('click', '.edit_btn', function (e) {

                      e.preventDefault();

                      $('.holiday-title').empty();
                      $('.holiday-btn').empty();
                      $('.holiday-title').html('Edit Holiday');
                      $('.holiday-btn').html('Update');

                      $('#id_value').empty();
                      $('.holiday_name').empty();
                      $('.holiday_date').empty();
                      $('input:radio[name="is_active"]').removeAttr('checked');

                      var get_id = $(this).attr('id');
                      var explode = get_id.split('_');
                      var value_id = explode[2];

                      var ddate = $('#td_date_'+value_id).html();
                      $('.holiday_date').val(ddate);

                      var dname = $('#td_name_'+value_id).html();
                      $('.holiday_name').val(dname);

                      var dstatus = $('#td_status_'+value_id).html();

                      if(dstatus == 1){
                        $('input:radio[name="is_active"][value="1"]').attr('checked',true);
                      }else{
                        $('input:radio[name="is_active"][value="0"]').attr('checked',true);
                      }

                      $('#id_value').val(value_id);
                      

                    });

                    $(document).on('click', '.holiday-btn', function (e) {

                      e.preventDefault();

                      $("#error_msg").empty(); $("#succ_msg").empty();

                      var holiday_name = $('.holiday_name').val();
                      var holiday_date = $('.holiday_date').val();
                      var is_active = $("input[name='is_active']:checked").val();

                      var values = {};
                      values['holiday_name'] = holiday_name;
                      values['holiday_date'] = holiday_date;
                      values['is_active'] = is_active;

                      var value_id = $('#id_value').val();

                      console.log('value_id'+value_id);

                      if(value_id == "" || value_id == null || value_id == 0){
                        var url =  '/holiday_add';
                        var data = {
                              "_token": "{{ csrf_token() }}",
                              "values": values,
                              "branch_id": "@if(isset($branch)) {{$branch->id}} @endif"
                            };
                      }else{
                        var url =  '/holiday_update';
                        var data = {
                              "_token": "{{ csrf_token() }}",
                              "values": values,
                              "branch_id": "@if(isset($branch)) {{$branch->id}} @endif",
                              "value_id": value_id
                            };
                      }

                      if(holiday_name == null || holiday_name == ""){
                        $("#error_msg").html('Please enter holiday name');
                      }else if(holiday_date == null || holiday_date == ""){
                        $("#error_msg").html('Please enter holiday date');
                      }else{

                        $('#pre-load').show();
                        $.ajax({
                            type: "post",
                            url: url,
                            data: data,
                            cache: false,
                            success: function(data) {
                                $('#pre-load').hide();
                                if(data.code == 200){
                                  $("#succ_msg").html('Holiday updated successfully');
                                  $(location).attr('href','/branch/holidays/{{base64_encode($branch->id)}}');
                                }
                            }
                        });
                        
                      }
                                             
                    });

                    
                });
            });
          </script>
@include('v1.footer')