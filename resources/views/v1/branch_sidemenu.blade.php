<div>
  <div class="list-group list-group-transparent mb-0">
    <a href="{{url('/branch/configs/'.base64_encode($branch->id).'')}}" class="list-group-item list-group-item-action d-flex align-items-center {{$active_configs}}">
      <span class="icon mr-3">
        <i class="fe fe-inbox"></i></span>
        Attendance configuration 
        <!-- <span class="ml-auto">
          <div class="mx-auto chart-circle chart-circle-xs" data-value="1" data-thickness="3" data-color="blue">
            <canvas width="20" height="20"></canvas>
            <div class="chart-circle-value">100%</div>
          </div>
        </span> -->
    </a>
    <a href="{{url('/branch/shifts/'.base64_encode($branch->id))}}" class="list-group-item list-group-item-action d-flex align-items-center {{$active_shifts}}">
      <span class="icon mr-3">
        <i class="fe fe-send"></i></span>
        Shifts
        <!-- <span class="ml-auto">
          <div class="mx-auto chart-circle chart-circle-xs" data-value="1" data-thickness="3" data-color="blue">
            <canvas width="20" height="20"></canvas>
            <div class="chart-circle-value">100%</div>
          </div>
        </span> -->
    </a>
    <a href="{{url('/branch/designation/'.base64_encode($branch->id))}}" class="list-group-item list-group-item-action d-flex align-items-center {{$active_designation}}">
      <span class="icon mr-3">
        <i class="fe fe-alert-circle"></i></span>
        Designation
        <!-- <span class="ml-auto">
          <div class="mx-auto chart-circle chart-circle-xs" data-value="1" data-thickness="3" data-color="blue">
            <canvas width="20" height="20"></canvas>
            <div class="chart-circle-value">100%</div>
          </div>
        </span> -->
    </a>
    <a href="{{url('/branch/holidays/'.base64_encode($branch->id))}}" class="list-group-item list-group-item-action d-flex align-items-center {{$active_holidays}}">
      <span class="icon mr-3">
        <i class="fe fe-star"></i></span>
        Holidays
        <!-- <span class="ml-auto">
          <div class="mx-auto chart-circle chart-circle-xs" data-value="1" data-thickness="3" data-color="blue">
            <canvas width="20" height="20"></canvas>
            <div class="chart-circle-value">100%</div>
          </div>
        </span> -->
    </a>
   <!--  <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
      <span class="icon mr-3">
        <i class="fe fe-file"></i></span>Drafts
    </a>
    <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
      <span class="icon mr-3">
        <i class="fe fe-tag"></i></span>Tags
    </a> -->
    <a href="{{url('/branch/edit/'.base64_encode($branch->id))}}" class="list-group-item list-group-item-action d-flex align-items-center {{$active_update}}">
      <span class="icon mr-3">
        <i class="fe fe-settings"></i></span>Update branch
        <!-- <span class="ml-auto">
          <div class="mx-auto chart-circle chart-circle-xs" data-value="1" data-thickness="3" data-color="blue">
            <canvas width="20" height="20"></canvas>
            <div class="chart-circle-value">100%</div>
          </div>
        </span> -->
    </a>

    

  </div>
  <!---->
  <div class="clearfix">
    <div class="float-left">
      <strong>96%</strong>
    </div>
    <div class="float-right">
      <small class="text-muted">Branch master setting</small>
    </div>
  </div>
  <div class="progress progress-xs">
    <div class="progress-bar bg-green" role="progressbar" style="width: 96%" aria-valuenow="96" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <p>If the master setting 100% complete to go access the employee adding and attendance</p>
  <!---->
</div>