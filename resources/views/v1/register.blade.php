@include('v1.header')
  
  <body class="">
    <div class="page" style="background-color: #fff;">
      <div class="page-single">
        <div class="container">
          <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6">
                <img src="{{asset('assets/images/logo/logo_1.png')}}" class="h-6" alt=""  style="background-color: #fff; height: 8rem !important;" >
              </div>
              <form class="card" action="" method="post" id="reg_form">
                <div class="card-body p-6">
                  <div class="card-title">Share your information</div>
                  <div class="error-msg align-center">
                      <p id="error_msg" style="color:red;"></p>
                      <p id="succ_msg" style="color:green;"></p>
                  </div>
                  <div class="form-group">
                    <label class="form-label">Name</label>
                    <input type="text" name="customer_name" class="form-control" id="exampleInputName"placeholder="Enter Name / Organization name">
                  </div>
                  <div class="form-group">
                    <label class="form-label">Email</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Email">
                  </div>
                  <div class="form-group">
                    <label class="form-label">Mobile</label>
                    <input type="text" name="mobile" class="form-control" id="exampleInputMobile" placeholder="Enter Mobile Number">
                  </div>
                  <div class="form-group">
                    <label class="form-label">
                      Password
                    </label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword" placeholder="Password">
                  </div>
                  <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block signup_btn">Sign in</button>
                  </div>
                </div>
              </form>
              <!-- <div class="text-center text-muted">
                Don't have account yet? <a href="./register.html">Sign up</a>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>

  <script type="text/javascript">

    require(['jquery'], function ($, selectize) {
      $(document).ready(function () {

            function ValidateEmail(mail) 
            {
             if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
              {
                return (true)
              }
                //alert("You have entered an invalid email address!")
                return (false)
            }

            $(document).on('click', '.signup_btn', function (event) {
              event.preventDefault();
              
              $('#error_msg').empty();$('#succ_msg').empty();

              var values = {};
              $.each($('#reg_form').serializeArray(), function(i, field) {
                  values[field.name] = field.value;
              });

              if(values.email != "" && values.password != ""){

                  if(ValidateEmail(values.email)){

                    $.ajax({
                      type: "post",
                      url: "/customer_add",
                      data: {
                              "_token": "{{ csrf_token() }}",
                              "values": values
                            },
                      cache: false,
                      success: function(data){
                          console.log(data);
                          if(data.code == 200){
                              $('#succ_msg').html(data.response);
                              // $(location).attr('href','/console');
                          }else{
                                // var err = data.error;
                                // email_err = email[0]
                                $('#error_msg').html(data.error);
                          }

                      }
                    });

                  }else{
                    $('#error_msg').html("You have entered an invalid email address!");
                  }

                  
              }else{
                  $('#error_msg').html("Please fill email and password");
              }   

              
            });

      });
    });

    
  </script>

    
@include('v1.footer')