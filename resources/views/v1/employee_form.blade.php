

                @if(isset($update))
                    @php $btn_name = "Update"; $submit_value = 1; $display = "";  @endphp
                @else
                    @php $btn_name = "Create"; $submit_value = 0; $display = "display: none"; @endphp
                @endif

                @php $ychecked = ""; $nchecked = 'checked'; @endphp

                <div class="row"><!--Start row-->                  

                  <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                        <label class="form-label">First name</label>
                        <input type="text" class="form-control first_name" name="first_name" placeholder="Jack" value="@if(isset($employee)) {{$employee->first_name}} @endif">
                        <div class="invalid-feedback first_name_err">Invalid feedback</div>
                    </div>
                  </div>

                  <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                        <label class="form-label">Last name</label>
                        <input type="text" class="form-control last_name" name="last_name" placeholder="Jones" value="@if(isset($employee)) {{$employee->last_name}} @endif" >
                        <div class="invalid-feedback last_name_err">Invalid feedback</div>
                    </div>
                  </div>

                  <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                      <label class="form-label">Date of birth</label>
                      <input type="text" class="form-control dob" name="dob" data-mask="00/00/0000" data-mask-clearifnotmatch="true" placeholder="DD/MM/YYYY" autocomplete="off" maxlength="10" value="@if(isset($employee)) {{date('d/m/Y' ,strtotime($employee->dob))}} @endif">
                      <div class="invalid-feedback dob_err">Invalid feedback</div>
                    </div>
                  </div>

                  <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                      <label class="form-label">Email</label>
                      <input type="text" class="form-control emp_email" name="emp_email" placeholder="jack@saturn1.com" value="@if(isset($employee)) {{$employee->emp_email}} @endif">
                      <div class="invalid-feedback emp_email_err">Invalid feedback</div>
                    </div>
                  </div>

                  <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                      <label class="form-label">Mobile</label>
                      <input type="text" class="form-control emp_mobile" name="emp_mobile" data-mask="0000000000" data-mask-clearifnotmatch="true" placeholder="0987654321" autocomplete="off" maxlength="10" value="@if(isset($employee)) {{$employee->emp_mobile}} @endif">
                      <div class="invalid-feedback emp_mobile_err">Invalid feedback</div>
                    </div>
                  </div>

                  <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                      <label class="form-label">Blood Group</label>
                      <input type="text" class="form-control blood_group" name="blood_group" placeholder="B Positve" value="@if(isset($employee)) {{$employee->blood_group}} @endif">
                      <div class="invalid-feedback blood_group_err">Invalid feedback</div>
                    </div>
                  </div>

                  <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                      <label class="form-label">Permanent Address</label>
                      <textarea class="form-control" rows="4" name="permanent_addr">@if(isset($employee)) {{$employee->permanent_addr}} @endif</textarea>
                    </div>
                  </div>

                  <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                      <label class="form-label">Present Address</label>
                      <textarea class="form-control" rows="4" name="present_addr">@if(isset($employee)) {{$employee->present_addr}} @endif</textarea>
                    </div>
                  </div>

                </div>
                <!--END-->

                <div class="row"><!--Stat row-->

                  <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                        <label class="form-label">Branch name</label>
                        <select name="branch_id"  class="form-control custom-select branch_id">
                          <option value="">Select Branch</option>
                          
                          @if(isset($branches))
                          @php $bselected = ""; @endphp
                          @foreach($branches as $branch)
                          @if(isset($employee)) 
                            @if($branch->id == $employee->branch_id) 
                                @php $bselected = "selected"; @endphp
                            @else
                                @php $bselected = ""; @endphp
                            @endif
                          @endif
                          <option value="{{$branch->id}}" {{$bselected}}>{{$branch->branch_name}}</option>
                          @endforeach

                          @endif

                        </select>
                      </div>
                  </div>

                  <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                      <label class="form-label">Employee Code</label>
                      <input type="text" class="form-control emp_code" name="emp_code" placeholder="SQS0901" value="@if(isset($employee)) {{$employee->emp_code}} @endif">
                      <div class="invalid-feedback emp_code_err">Invalid feedback</div>
                    </div>
                  </div>

                  <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                      <label class="form-label">Date of joining</label>
                      <input type="text" class="form-control doj" name="doj" data-mask="00/00/0000" data-mask-clearifnotmatch="true" placeholder="DD/MM/YYYY" autocomplete="off" maxlength="10" value="@if(isset($employee)) {{date('d/m/Y' ,strtotime($employee->doj))}} @endif">
                      <div class="invalid-feedback doj_err">Invalid feedback</div>
                    </div>
                  </div>


                  <div class="col-md-4 col-lg-4 branch_configs" style="{{$display}}">
                    <div class="form-group">
                        <label class="form-label">Designation</label>
                        <select name="designation_id" id="designation_id"  class="form-control custom-select designation_id">
                          <option value="">Select Designation</option>
                        </select>
                      </div>
                  </div>

                  <div class="col-md-4 col-lg-4 branch_configs" style="{{$display}}">
                    <div class="form-group">
                        <label class="form-label">Shifts</label>
                        <select name="shift_id" id="shift_id"  class="form-control custom-select shift_id">
                          <option value="">Select Shifts</option>
                        </select>
                      </div>
                  </div>

                  <div class="col-md-4 col-lg-4 branch_configs" style="{{$display}}">
                    <div class="form-group">
                      <label class="form-label">Reporting Head</label>
                      <select name="reporting_head_id" id="reporting_head_id" class="form-control custom-select reporting_head_id">                      
                      </select>
                    </div>
                  </div>

                  
                  @if(isset($employee)) 
                    @if($employee->is_reporting_head == 1)
                      @php $ychecked = "checked"; $nchecked = ''; @endphp
                    @else
                      @php $ychecked = ""; $nchecked = 'checked'; @endphp
                    @endif
                  @endif

                  <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                      <label class="form-label">Is Reorting head</label>
                      <div class="selectgroup w-100">
                        <label class="selectgroup-item">
                          <input type="radio" name="is_reporting_head" value="1" class="selectgroup-input" {{$ychecked}}> <span class="selectgroup-button">Yes</span>
                        </label>
                        <label class="selectgroup-item">
                          <input type="radio" name="is_reporting_head" value="0" class="selectgroup-input" {{$nchecked}}> <span class="selectgroup-button">No</span>
                        </label>
                      </div>
                    </div>
                  </div>

                  


                </div><!--End row-->

              

              <input type="hidden" id="shift_edit_id" value="@if(isset($employee)) {{$employee->shift_id}} @endif" >
              <input type="hidden" id="designation_edit_id" value="@if(isset($employee)) {{$employee->designation_id}} @endif" >
              <input type="hidden" id="reporting_head_edit_id" value="@if(isset($employee)) {{$employee->reporting_head_id}} @endif" >

              <div class="d-flex">
                  <button type="submit" id="employee_create" class="btn btn-primary ml-auto">{{$btn_name}}</button>
              </div>   

                   

            