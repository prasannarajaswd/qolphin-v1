<body class="">

    <div id="pre-load" style="display: none;"></div>

    @if(isset($active_o)) @php $active_o = $active_o;@endphp
    @else @php $active_o = ""; @endphp
    @endif

    @if(isset($active_b)) @php $active_b = $active_b;@endphp
    @else @php $active_b = ""; @endphp
    @endif

    @if(isset($active_e)) @php $active_e = $active_e;@endphp
    @else @php $active_e = ""; @endphp
    @endif

    <div class="page" style="background-color: #fff;">
      <div class="flex-fill" style="margin: 5px;">
        <div class="header py-4">
          <div class="container" style="max-width: 100%;">
            <div class="d-flex">
              <a class="header-brand" href="{{url('/console')}}">
                <img src="{{asset('assets/images/logo/logo_1.png')}}" class="header-brand-img" alt="qolphin logo">
              </a>
              <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                  <li class="nav-item">
                    <a href="{{url('console')}}" class="nav-link {{$active_o}}"><i class="fe fe-box"></i> Overview</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{url('branch')}}" class="nav-link {{$active_b}}" ><i class="fe fe-sliders"></i> Branch</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a href="{{url('employees')}}" class="nav-link {{$active_e}}"><i class="fe fe-users"></i> Employees</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a href="#!" class="nav-link "><i class="fe fe-calendar"></i> Payroll</a>
                  </li>
                  <!--  -->
                </ul>
              </div>

              
              <div class="d-flex order-lg-2 ml-auto">
                <div class="nav-item d-none d-md-flex" style="color: #562e91;padding: 15px;">
                  {{Auth::user()->name}}
                </div>
                <div class="dropdown d-none d-md-flex">
                  <a class="nav-link icon" data-toggle="dropdown">
                    <i class="fe fe-bell"></i>
                    <span class="nav-read"></span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    Empty
                    <!-- <a href="#" class="dropdown-item d-flex">
                      <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/male/41.jpg)"></span>
                      <div>
                        <strong>Nathan</strong> pushed new commit: Fix page load performance issue.
                        <div class="small text-muted">10 minutes ago</div>
                      </div>
                    </a>
                    <a href="#" class="dropdown-item d-flex">
                      <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/female/1.jpg)"></span>
                      <div>
                        <strong>Alice</strong> started new task: Tabler UI design.
                        <div class="small text-muted">1 hour ago</div>
                      </div>
                    </a>
                    <a href="#" class="dropdown-item d-flex">
                      <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/female/18.jpg)"></span>
                      <div>
                        <strong>Rose</strong> deployed new version of NodeJS REST Api V3
                        <div class="small text-muted">2 hours ago</div>
                      </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item text-center">Mark all as read</a> -->
                  </div>
                </div>
                <div class="dropdown">
                  <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                    <span class="avatar" style="background-image: url(https://cdn2.iconfinder.com/data/icons/funtime-animals-humans/60/004_005_dolphin_sea_ocean_happy_friend_animal_mammal-512.png)"></span>
                    <!-- <span class="ml-2 d-none d-lg-block">
                      <span class="text-default">{{Auth::user()->name}}</span>
                      <small class="text-muted d-block mt-1">
                        @if(Auth::user()->user_type == "C")
                          Customer Admin
                        @else
                          Administrator
                        @endif
                      </small>
                    </span> -->
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fe fe-user"></i> Profile
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fe fe-settings"></i> Settings
                    </a>
                    
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fe fe-help-circle"></i> Need help?
                    </a>
                    <a class="dropdown-item" href="{{ url('/logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                       <i class="dropdown-icon fe fe-log-out"></i> Sign out
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                  </div>
                </div>
              </div>
              <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
              </a>
            </div>
          </div>
        </div>

        @if(isset($breadcrumbs))
          @php $breadcrumbs = $breadcrumbs; @endphp
        @else
          @php $breadcrumbs = ""; @endphp
        @endif

        <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
          <div class="container">
            <div class="row align-items-left">
              <div class="col-lg-3 ml-auto breadcrumbs">
                <a href="#!" class="alert-link"> <strong>You Are In :</strong> {{$breadcrumbs}} <span>/</span> </a>
              </div>
              
            </div>
          </div>
        </div>
        
