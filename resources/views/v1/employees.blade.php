@include('v1.header')

  @php $breadcrumbs = "Employees"; $active_e = "active"; @endphp
  @include('v1.menu')

  

    <div class="my-3 my-md-5">
      <div class="container">
       
        <div class="page-header">
          
         
          <div class="ml-auto" style="text-align: right;"> 
            <a href="{{ url('/employees/add') }}" class="btn btn-primary"><i class="fe fe-plus"></i> Bulk Add</a>
            <a href="{{ url('/employees/add') }}" class="btn btn-primary"><i class="fe fe-plus"></i> Add</a>
          </div>
        </div>

        <!--START-->

        <div class="row row-cards row-deck">
          <div class="col-12">
            <div class="card" style="background-color: transparent;">
              <div class="table-responsive">
                <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                  <thead>
                    <tr>
                      <th class="text-center w-1"><i class="icon-people"></i>
                      </th>
                      <th>Employee name</th>
                      <th>Branch</th>
                      <th>Contact details</th>
                      <th></th>
                      <th>Reporting Head</th>
                      <th>Shift</th>
                      <th>Date of joining</th>
                      <th>Status</th>
                      <!-- <th>Onboarding</th> -->
                      <!-- <th class="text-center">More Actions</th> -->
                      <th></th>
                      </th>
                    </tr>
                  </thead>
                  <tbody style="font-size: 13px;">
                    @if(isset($employees))

                    @foreach($employees as $employee)
                    <tr>
                      <td class="text-center">

                        @if($employee->profile_pic == null || $employee->profile_pic == "")
                          @php $pic = "https://qolphin-assets.s3.us-east-2.amazonaws.com/default_image.png"; @endphp
                        @else
                          @php $pic = $employee->profile_pic; @endphp
                        @endif

                        <div class="avatar d-block" style="background-image: url({{$pic}})"> 
                          @if($employee->profile_pic_status == 1)
                          <span class="avatar-status bg-green"></span>
                          @elseif($employee->profile_pic_status == 0)
                          <span class="avatar-status bg-grey"></span>
                          @elseif($employee->profile_pic_status == 2)
                          <span class="avatar-status bg-yellow"></span>
                          @else
                          <span class="avatar-status bg-red"></span>
                          @endif
                          
                        </div>
                      </td>
                      <td>
                        <input type="hidden" id="emp_id_{{$employee->id}}">
                        <div>{{$employee->first_name}} {{$employee->last_name}}</div>
                        <div class="small text-muted">{{$employee->designation_name}}</div>
                      </td>
                      <td>
                        <div>{{$employee->branch_name}}</div>
                        <div class="small text-muted">created date: {{date('M d, Y',strtotime($employee->created_at))}}</div>
                      </td>
                      <td>
                        <div class="clearfix">
                          <div class="float-left"> <strong>{{$employee->emp_mobile}}</strong>
                          </div>
                          <div class="float-right"> <small class="text-muted">{{$employee->emp_code}}</small>
                          </div>
                        </div>
                        <div>
                          <small class="text-muted">{{$employee->emp_email}}</small>
                        </div>
                      </td>

                      <td class="text-center">

                        @if($employee->rh_pic == null || $employee->rh_pic == "")
                          @php $rh_pic = "https://qolphin-assets.s3.us-east-2.amazonaws.com/default_image.png"; @endphp
                        @else
                          @php $rh_pic = $employee->rh_pic; @endphp
                        @endif

                        @if($employee->rh_fname != "")
                        <div class="avatar d-block" style="background-image: url({{$rh_pic}})"></div>
                        @endif
                                                
                      </td>
                      <td>
                        <div>{{$employee->rh_fname}} {{$employee->rh_lname}}</div>
                      </td>

                      <td>
                        <div>{{$employee->shift_name}}</div>
                        <div class="small text-muted">{{$employee->start_time}} {{$employee->end_time}}</div>
                      </td>
                      <td>
                        <div>{{date('M d, Y', strtotime($employee->doj))}}</div>
                      </td>
                      <td>

                        @if($employee->is_active == 1)
                          @php $status = "Active"; $color = "green";  @endphp
                        @elseif($employee->is_active == 0)
                          @php $status = "InActive"; $color = "red";  @endphp
                        @elseif($employee->is_active == 2)
                          @php $status = "Resigned"; $color = "grey";  @endphp
                        @else
                          @php $status = "Pending verify"; $color = "yellow";  @endphp
                        @endif

                        <input type="hidden" id="is_active_{{$employee->id}}" value="{{$employee->is_active}}">

                        <div class="clearfix">
                          <div> <strong>{{$status}}</strong></div>
                        </div>
                        <div class="progress progress-xs">
                          <div class="progress-bar bg-{{$color}}" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </td>

                      <!-- <td class="text-center">
                        <div class="mx-auto chart-circle chart-circle-xs" data-value="0.42" data-thickness="3" data-color="blue">
                          <canvas width="40" height="40"></canvas>
                          <div class="chart-circle-value">42%</div>
                        </div>
                      </td> -->
                      <td class="text-center">
                        <div class="item-action dropdown"> 
                          <a href="javascript:void(0)" data-toggle="dropdown" class="icon"><i class="fe fe-more-vertical"></i>
                          </a>
                          <div class="dropdown-menu dropdown-menu-right"> 
                            <a href="{{url('/employees/edit/'.base64_encode($employee->id))}}" class="dropdown-item"><i class="dropdown-icon fe fe-tag"></i> Edit 
                            </a>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#ChangeStatus" class="dropdown-item change_status" data-id="{{$employee->id}}"><i class="dropdown-icon fe fe-edit-2"></i> Change Status
                            </a>
                            <a href="{{url('/employees/view/'.base64_encode($employee->id))}}" class="dropdown-item"><i class="dropdown-icon fe fe-clock"></i> View Attendance
                            </a>
                            <div class="dropdown-divider"></div> 
                            <a href="javascript:void(0)" class="dropdown-item"><i class="dropdown-icon fe fe-message-square"></i> Send notification
                            </a>
                          </div>
                        </div>
                      </td>
                    </tr>
                    @endforeach

                    @endif
                    
                    
                    
                    
                  </tbody>
                </table>
              </div>

              <nav aria-label="Page navigation example">
                <ul class="pagination pagination-sm justify-content-center">
                  <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                      <span class="sr-only">Previous</span>
                    </a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                      <span class="sr-only">Next</span>
                    </a>
                  </li>
                </ul>
              </nav>

            </div>
          </div>
        </div>

        <!--END-->
        
      </div>
    </div>

    <div id="ChangeStatus" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title emp-name">Prasanna Raja</h4><br>
            <button type="submit" class="btn btn-primary ml-auto status-btn">Change</button>
          </div>
          <div class="modal-body">

              <div class="error-msg align-center">
                  <p id="error_msg" style="color:red;"></p>
                  <p id="succ_msg" style="color:green;"></p>
              </div>
              
              <input type="text" name="id_value" id="id_value" hidden>

              <div class="form-group">
                <label class="form-label">Status Changing</label>
                <div class="selectgroup selectgroup-vertical w-100">
                  <label class="selectgroup-item">
                    <input type="radio" name="status_is_active" value="1" class="selectgroup-input">
                    <span class="selectgroup-button">Active</span>
                  </label>
                  <label class="selectgroup-item">
                    <input type="radio" name="status_is_active" value="0" class="selectgroup-input">
                    <span class="selectgroup-button">Inactive</span>
                  </label>
                  <label class="selectgroup-item">
                    <input type="radio" name="status_is_active" value="2" class="selectgroup-input">
                    <span class="selectgroup-button">Resigned</span>
                  </label>
                </div>
              </div>
              

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>

    


    <script type="text/javascript">

            require(['input-mask']);

            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            
            require(['jquery', 'selectize'], function ($, selectize) {
                $(document).ready(function () {

                  $(document).on('click', '.change_status', function (e) {
                    var id = $(this).data("id");

                    var exist_val = $('#is_active_'+id).val();

                    $('#id_value').val('');

                    $('#id_value').val(id);
                    $('input:radio[name="status_is_active"]').removeAttr('checked');
                    $('input:radio[name="status_is_active"][value="'+exist_val+'"]').attr('checked',true);

                  });
                   
                  $(document).on('click', '.status-btn', function () {

                    var id_value = $('#id_value').val();
                    var is_active = $("input[name='status_is_active']:checked").val();

                    $('#pre-load').show();
                    $.ajax({
                        type: "post",
                        url: '/employee_change_status',
                        data: {
                              "_token": "{{ csrf_token() }}",
                              "is_active": is_active,
                              "value_id": id_value
                            },
                        cache: false,
                        success: function(data) {
                            $('#pre-load').hide();
                            if(data.code == 200){
                              $("#succ_msg").html('Status updated successfully');
                              location.reload();
                            }
                        }
                    });

                  });
                    
                });
            });
          </script>
@include('v1.footer')