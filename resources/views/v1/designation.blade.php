@include('v1.header')

  @php $breadcrumbs = "Branches / Designation"; @endphp
  @include('v1.menu') 

    <div class="my-3 my-md-5">
      <div class="container">
       
        <div class="page-header">
          <!-- <div class="ml-auto" style="text-align: right;"> 
            <a href="{{ url('/branch/add') }}" class="btn btn-primary"><i class="fe fe-plus"></i> New</a>
          </div> -->
        </div>

          <div class="row">

              <div class="col-md-3">
                    
                  @php 
                    $active_configs = ""; $active_shifts = ""; $active_designation = "active";
                    $active_holidays = ""; $active_update = "";
                  @endphp

                  @include('v1.branch_sidemenu')

              </div>
              <div class="col-md-9">
                <!--start--->
                  
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Designation</h3>
                    <button type="submit" class="btn btn-primary ml-auto add_btn" data-toggle="modal" data-target="#AddDesignation">Add</button>
                  </div>
                  <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap datatable">
                      <thead>
                        <tr>
                          <th class="w-1">No.</th>
                          <th>Designation name</th>
                          <th hidden></th>
                          <th>Status</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        
                        @if(isset($designations))

                          @foreach($designations as $k => $designation)
                          <tr>

                            @php $k = $k+1; @endphp
                            <td>{{$k}}</td>
                            <td id="td_name_{{$designation->id}}">{{$designation->designation_name}}</td>
                            <td hidden id="td_status_{{$designation->id}}">{{$designation->is_active}}</td>
                            <td>
                            @if($designation->is_active == 1)
                            Active
                            @else
                            Inactive
                            @endif
                            </td>
                            <td class="text-right">
                              <a href="javascript:void(0)" id="td_edit_{{$designation->id}}" class="btn btn-secondary btn-sm edit_btn" data-toggle="modal" data-target="#AddDesignation">Edit</a>
                              <a href="javascript:void(0)" class="btn btn-secondary btn-sm">Delete</a>
                            </td>
                         

                          </tr>
                          @endforeach


                        @endif
                        
                      </tbody>
                    </table>
                    <script>
                      require(['datatables', 'jquery'], function(datatable, $) {
                            $('.datatable').DataTable({
                              "bPaginate": false,
                              "pageLength": 5
                            });
                          });
                    </script>
                  </div>
                </div>

                <!--end--->

              </div>  

          </div>
          
        
      </div>
    </div>

    <div id="AddDesignation" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title designation-title">Add Designation</h4>
            <button type="submit" class="btn btn-primary ml-auto designation-btn">Save</button>
          </div>
          <div class="modal-body">

              <div class="error-msg align-center">
                  <p id="error_msg" style="color:red;"></p>
                  <p id="succ_msg" style="color:green;"></p>
              </div>
              
              <input type="text" name="id_value" id="id_value" hidden>

              <div class="form-group">
                <label class="form-label">Designation Name</label>
                <input type="text" class="form-control designation_name" name="designation_name" placeholder="Customer support executive">
              </div>
              <div class="form-group">
                <label class="form-label">Status</label>
                <div class="selectgroup w-100">
                  <label class="selectgroup-item">
                    <input type="radio" name="is_active" value="1" class="selectgroup-input" checked>
                    <span class="selectgroup-button">Active</span>
                  </label>
                  <label class="selectgroup-item">
                    <input type="radio" name="is_active" value="0" class="selectgroup-input">
                    <span class="selectgroup-button">Inactive</span>
                  </label>
                </div>
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>


    <script type="text/javascript">
            function isNumberKey(evt){
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            
            require(['jquery', 'selectize'], function ($, selectize) {
                $(document).ready(function () {

                    
                    $(document).on('click', '.add_btn', function (e) {

                      e.preventDefault();

                      $('.designation-title').empty();
                      $('.designation-btn').empty();
                      $('.designation-title').html('Add Designation');
                      $('.designation-btn').html('Save');

                      $('#id_value').empty();
                      $('.designation_name').empty();
                      $('input:radio[name="is_active"]').removeAttr('checked');
                      $('input:radio[name="is_active"][value="1"]').attr('checked',true);


                    });

                    $(document).on('click', '.edit_btn', function (e) {

                      e.preventDefault();

                      $('.designation-title').empty();
                      $('.designation-btn').empty();
                      $('.designation-title').html('Edit Designation');
                      $('.designation-btn').html('Update');

                      $('#id_value').empty();
                      $('.designation_name').empty();
                      $('input:radio[name="is_active"]').removeAttr('checked');

                      var get_id = $(this).attr('id');
                      var explode = get_id.split('_');
                      var value_id = explode[2];

                      var dname = $('#td_name_'+value_id).html();
                      $('.designation_name').val(dname);

                      var dstatus = $('#td_status_'+value_id).html();

                      if(dstatus == 1){
                        $('input:radio[name="is_active"][value="1"]').attr('checked',true);
                      }else{
                        $('input:radio[name="is_active"][value="0"]').attr('checked',true);
                      }

                      $('#id_value').val(value_id);
                      

                    });

                    $(document).on('click', '.designation-btn', function (e) {

                      e.preventDefault();

                      $("#error_msg").empty(); $("#succ_msg").empty();

                      var designation_name = $('.designation_name').val();
                      var is_active = $("input[name='is_active']:checked").val();

                      var values = {};
                      values['designation_name'] = designation_name;
                      values['is_active'] = is_active;

                      var value_id = $('#id_value').val();

                      console.log('value_id'+value_id);

                      if(value_id == "" || value_id == null || value_id == 0){
                        var url =  '/designation_add';
                        var data = {
                              "_token": "{{ csrf_token() }}",
                              "values": values,
                              "branch_id": "@if(isset($branch)) {{$branch->id}} @endif"
                            };
                      }else{
                        var url =  '/designation_update';
                        var data = {
                              "_token": "{{ csrf_token() }}",
                              "values": values,
                              "branch_id": "@if(isset($branch)) {{$branch->id}} @endif",
                              "value_id": value_id
                            };
                      }

                      if(designation_name == null || designation_name == ""){
                        $("#error_msg").html('Please enter designation name');
                      }else{

                        $('#pre-load').show();
                        $.ajax({
                            type: "post",
                            url: url,
                            data: data,
                            cache: false,
                            success: function(data) {
                                $('#pre-load').hide();
                                if(data.code == 200){
                                  $("#succ_msg").html('Designation updated successfully');
                                  $(location).attr('href','/branch/designation/{{base64_encode($branch->id)}}');
                                }
                            }
                        });
                        
                      }
                                             
                    });

                    
                });
            });
          </script>
@include('v1.footer')