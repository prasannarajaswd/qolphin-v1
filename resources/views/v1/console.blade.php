@include('v1.header')
  @php $breadcrumbs = "Console"; $active_o = "active";@endphp
  @include('v1.menu')
  <div class="my-3 my-md-5">
    <div class="container" style="    padding-left: 150px;
    padding-right: 150px;
    padding-top: 50px;
    padding-bottom: 50px;">
      
      <div class="row">
        <div class="col-sm-4 col-lg-4">
          <div class="card" style="background-color: transparent !important;">
            <div class="card-body text-center">
              <div class="card-category">Branches</div>
              <div class="display-3 my-4">{{$branch_ct}}</div>
              <ul class="list-unstyled leading-loose">
                <li>Create branch masters to access the employee and payroll process</li>
              </ul>
              <div class="text-center mt-6">
                <a href="{{url('/branch')}}" class="btn btn-secondary btn-block" style="border: none; background-color: #eeeff1;">Go Branches</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-lg-4">
          <div class="card" style="background-color: transparent !important;">
            <div class="card-body text-center">
              <div class="card-category">Users</div>
              <div class="display-3 my-4">{{$users_ct}}</div>
              <ul class="list-unstyled leading-loose">
                <li>Total number of users for mobile application both android and ios.</li>
              </ul>
              <div class="text-center mt-6">
                <a href="{{url('/employees')}}" class="btn btn-secondary btn-block" style="border: none;background-color: #eeeff1;">Go Employees</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-lg-4">
          <div class="card" style="background-color: transparent !important;">
            <div class="card-body text-center">
              <div class="card-category">Bill</div>
              <div class="display-3 my-4">&#8377; 0</div>
              <ul class="list-unstyled leading-loose">
                <li>Free: First 65 Days, then we will charge <br>&#8377; 1 / 1 USER / 1 DAY</li>
              </ul>
              <div class="text-center mt-6">
                <a href="#!" class="btn btn-secondary btn-block" style="border: none;background-color: #eeeff1;">Go Bill</a>
              </div>
            </div>
          </div>
        </div>
      </div> 

      
    </div>
  </div>
@include('v1.footer')