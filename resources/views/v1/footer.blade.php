      
      </div>
      <footer class="footer">
        <div class="container">
          <div class="row align-items-center flex-row-reverse">
            <div class="col-auto ml-lg-auto">
              <div class="row align-items-center">
                <div class="col-auto">
                  a product of
                  <a href="/saturnq.com" target="_blank" class="">Saturnq</a>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
              Copyright © {{date('Y')}} <a href="/qolphin.com" target="_blank">qolphin</a>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>
</html>