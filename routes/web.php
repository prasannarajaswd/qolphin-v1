<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
if (App::environment('production')) {
    URL::forceScheme('https');
}

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', 'PagesController@authentication');
Route::get('register', 'PagesController@registration');
Auth::routes();

Route::get('privacy-policy', 'PagesController@privacy');

//authentication
Route::get('authentication', 'PagesController@authentication');


//middleware auth
Route::group(['middleware' => ['auth']], function () {

	//branches
	Route::get('/branch', 'PagesController@branch');
	Route::get('/branch/add', 'PagesController@branch_adding');
	Route::post('/branch_add', 'API\BranchController@branch_add');
	Route::post('/branch_update', 'API\BranchController@branch_update');

	//branch masters
	Route::get('/branch/edit/{id}', 'PagesController@edit_branch');
	Route::get('/branch/configs/{id}', 'PagesController@attendance_configs');
	Route::get('/branch/shifts/{id}', 'PagesController@shifts');
	Route::get('/branch/designation/{id}', 'PagesController@designation');
	Route::get('/branch/holidays/{id}', 'PagesController@holidays');

	//employees
	Route::get('/employees/add', 'PagesController@employee_adding');
	Route::get('/employees', 'PagesController@employees');
	Route::get('/employees/edit/{id}', 'PagesController@employee_edit');
	Route::get('/employees/view/{id}', 'PagesController@employee_attendance');

	//employees add/update
	Route::post('employee_add', 'API\EmployeeController@employee_add');
	Route::post('employee_update', 'API\EmployeeController@employee_update');
	Route::post('employee_change_status', 'API\EmployeeController@employee_change_status');


	//get branch configs when adding employee
	Route::post('/get_branch_configs', 'API\BranchController@get_branch_configs');

	//designation post
	Route::post('/designation_add', 'API\BranchController@designation_add');
	Route::post('/designation_update', 'API\BranchController@designation_update');

	//shift post
	Route::post('/shift_add', 'API\BranchController@shift_add');
	Route::post('/shift_update', 'API\BranchController@shift_update');

	//holiday post
	Route::post('/holiday_add', 'API\BranchController@holiday_add');
	Route::post('/holiday_update', 'API\BranchController@holiday_update');

	//attendance configs
	Route::post('/attendance_config', 'API\BranchController@attendance_config');

	//my attendances
	Route::get('my_attendances_web', 'API\AttendanceController@my_attendances_web');
	

	//customer
	Route::post('customer_add', 'API\CustomerController@customer_add');
	Route::post('customer_register', 'API\CustomerController@customer_register');

	
	//overview
	Route::get('/console', 'PagesController@console');
	
});
	
Route::get('.well-known/acme-challenge/7ZUAGEBMFzfxwdd1FupDVlJ5j1jM6wOzjSIbzmaaIR0', 'PagesController@txt1');
Route::get('.well-known/acme-challenge/B6ebQGjY0c4inaNMrqQi2cC6N0QMKt2oHj1T01HAnuc', 'PagesController@txt2');
//overview
Route::get('/email_temp', 'PagesController@email_temp');



