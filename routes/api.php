<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//test
Route::get('details', 'API\UserController@details');

//Spalsh screen 

//login
Route::post('cin_validation', 'API\UserController@cin_validation');
Route::post('login', 'API\UserController@login');
Route::post('verify_otp', 'API\UserController@verify_otp');
Route::get('noti', 'API\UserController@noti');


Route::group(['middleware' => 'auth:api'], function(){

    //qpin
    Route::post('qpin_set', 'API\UserController@qpin_set');
    Route::post('qpin_verify', 'API\UserController@qpin_verify');

    //dashboard
	Route::post('dashboard', 'API\DashboardController@dashboard');

	//activities
	Route::post('activity_list', 'API\ActivitiesController@activity_list');
	Route::post('approval_list', 'API\ActivitiesController@approval_list');

	//my attendances
	Route::post('my_attendances', 'API\AttendanceController@my_attendances');

	//regularization 
	Route::post('attendance_regularize', 'API\AttendanceController@attendance_regularize');

	//approvals 
	Route::post('my_approvals_data', 'API\ApprovalsController@approvals_list');

	//approval_action
	Route::post('approval_action', 'API\ApprovalsController@approval_action');

	//approvals view
	Route::post('approval_view', 'API\ApprovalsController@approval_view');
	

	//profile
	Route::post('profile', 'API\ProfileController@profile');
	Route::post('upload_profile_pic', 'API\ProfileController@upload_profile_pic');

    //attendances initialize/submit
    Route::post('check_in_out_init', 'API\AttendanceController@check_in_out_init');
    Route::post('check_in_out_submit', 'API\AttendanceController@check_in_out_submit');

    //notification
    Route::post('notification', 'API\NotificationController@notification');
    Route::post('notification_change', 'API\NotificationController@change_status');
});

//Route::get('notification', 'API\NotificationController@notification');

Route::post('customer_add', 'API\CustomerController@customer_add');

//upload image
Route::post('upload_file', 'API\CommonController@upload_file');
Route::get('send_sms', 'API\CommonController@send_sms');




