<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMobileUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('mobile')->nullable();
            $table->string('country_code')->default('+91');
            $table->integer('is_active')->default('1');
            $table->string('otp')->nullable();
            $table->integer('is_verify_otp')->default('0');
            $table->string('qpin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
