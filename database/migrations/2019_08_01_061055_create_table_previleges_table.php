<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrevilegesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('previleges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('previlege')->nullabel();
            $table->string('previlege_type')->nullable();
            $table->integer('is_active')->default('1');
            $table->timestamps();
        });
    }

    // insert into previleges (previlege, previlege_type) values ('Do you want checkin in mobile app', 'checkin');
    // insert into previleges (previlege, previlege_type) values ('Do you want checkout in mobile app', 'checkout');

    // insert into previleges (previlege, previlege_type) values ('Do you want check gps during in checkin time', 'checkin_gps');
    // insert into previleges (previlege, previlege_type) values ('Do you want check gps during in checkout time', 'checkout_gps');

    // insert into previleges (previlege, previlege_type) values ('Do you want take selfie during in checkin time', 'checkin_selfie');
    // insert into previleges (previlege, previlege_type) values ('Do you want take selfie during in checkout time', 'checkout_selfie');

    // insert into previleges (previlege, previlege_type) values ('Do you want compare face during in checkin time', 'checkin_face');
    // insert into previleges (previlege, previlege_type) values ('Do you want compare face during in checkout time', 'checkout_face');

    // insert into previleges (previlege, previlege_type) values ('Do you want question raise during in checkin time', 'checkin_question');
    // insert into previleges (previlege, previlege_type) values ('Do you want question raise during in checkout time', 'checkout_question');
    
    // insert into previleges (previlege, previlege_type, during_process) values ('Do you want tl approve attendance', 'tl_approve_attendance', 'attendance');
    // insert into previleges (previlege, previlege_type, during_process) values ('Do you want tl approve attendance', 'tl_approve_attendance', 'attendance');
    
    // update previleges set during_process = 'checkout' where id in (4, 6, 8, 10);
    // update previleges set during_process = 'checkin' where id in (4, 6, 8, 10);
   

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('previleges');
    }
}
