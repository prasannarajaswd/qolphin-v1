<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer_name');
            $table->integer('user_id');
            $table->string('email');
            $table->string('mobile');
            $table->string('alt_mobile')->nullable();
            $table->string('addresss_1')->nullable();
            $table->string('addresss_2')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->string('company_proof')->nullable();
            $table->integer('is_verify_email')->default('0');
            $table->integer('is_verify_mobile')->default('0');
            $table->string('is_active')->default('1');
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
