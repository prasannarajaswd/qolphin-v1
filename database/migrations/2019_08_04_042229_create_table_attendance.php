<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAttendance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        // 'employee_id' => 'required',
        // 'user_id' => 'required',
        // 'log_type' => 'required',
        // 'attendance_date' => 'required',
        Schema::create('attendances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('employee_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('attendance_date')->nullable();
            $table->time('checkin_time')->nullable();
            $table->time('checkout_time')->nullable();
            $table->string('checkin_lat')->nullable();
            $table->string('checkin_lng')->nullable();
            $table->string('checkout_lat')->nullable();
            $table->string('checkout_lng')->nullable();
            $table->string('checkin_distance')->nullable();
            $table->string('checkout_distance')->nullable();
            $table->string('checkin_selfie')->nullable();
            $table->string('checkout_selfie')->nullable();
            $table->string('checkin_face')->nullable();
            $table->string('checkout_face')->nullable();
            $table->string('attendance_status')->nullable();
            $table->integer('approval_status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
