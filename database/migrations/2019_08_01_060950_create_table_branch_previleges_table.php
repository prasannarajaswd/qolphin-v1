<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBranchPrevilegesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_previleges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('branch_id');
            $table->integer('customer_id');
            $table->integer('previlege_id');
            $table->integer('answer');
            $table->timestamps();
        });
    }

    // insert into branch_previleges (branch_id, customer_id, previlege_id, answer) values (1, 2, 1, 1 );
    // insert into branch_previleges (branch_id, customer_id, previlege_id, answer) values (1, 2, 3, 1 );

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_previleges');
    }
}
