<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id');
            $table->integer('branch_id');
            $table->integer('user_id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->integer('dob')->nullable();
            $table->string('emp_email')->nullable();
            $table->string('emp_mobile')->nullable();
            $table->string('emp_code')->nullable();
            $table->string('profile_pic')->nullable();
            $table->text('permanent_addr')->nullable();
            $table->text('present_addr')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('reporting_head_id')->nullable();
            $table->integer('is_reporting_head')->default('0');
            $table->integer('shift_id')->nullable();
            $table->integer('doj')->nullable();
            $table->integer('is_active')->default('1');
            $table->timestamps();
        });
    }    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
