<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAttendanceApprovalLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_approval_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('attendance_id')->default('0');
            $table->integer('checkin_approval')->default('0');
            $table->integer('checkin_approved_by')->default('0');
            $table->datetime('checkin_approved_date')->nullable();
            $table->integer('checkout_approval')->default('0');
            $table->integer('checkout_approved_by')->default('0');
            $table->datetime('checkout_approved_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_approval_logs');
    }
}
