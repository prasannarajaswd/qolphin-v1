<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('previleges')->insert([ [
            'previlege' => 'Do you want checkin in mobile app',
            'previlege_type' => 'checkin',
            'during_process' => 'checkin',
        ], [
            'previlege' => 'Do you want checkout in mobile app',
            'previlege_type' => 'checkout',
            'during_process' => 'checkout',
        ], [
            'previlege' => 'Do you want check gps during in checkin time',
            'previlege_type' => 'checkin_gps',
            'during_process' => 'checkin',
        ], [
            'previlege' => 'Do you want check gps during in checkout time',
            'previlege_type' => 'checkout_gps',
            'during_process' => 'checkout',
        ], [
            'previlege' => 'Do you want take selfie during in checkin time',
            'previlege_type' => 'checkin_selfie',
            'during_process' => 'checkin',
        ], [
            'previlege' => 'Do you want take selfie during in checkout time',
            'previlege_type' => 'checkout_selfie',
            'during_process' => 'checkout',
        ], [
            'previlege' => 'Do you want compare face during in checkin time',
            'previlege_type' => 'checkin_face',
            'during_process' => 'checkin',
        ], [
            'previlege' => 'Do you want compare face during in checkout time',
            'previlege_type' => 'checkout_face',
            'during_process' => 'checkout',
        ], [
            'previlege' => 'Do you want question raise during in checkin time',
            'previlege_type' => 'checkin_question',
            'during_process' => 'checkin',
        ], [
            'previlege' => 'Do you want question raise during in checkout time',
            'previlege_type' => 'checkout_question',
            'during_process' => 'checkout',
        ], [
            'previlege' => 'Do you want tl approve attendance',
            'previlege_type' => 'tl_approve_attendance',
            'during_process' => 'attendance',
        ] ]);

        DB::table('users')->insert([
            'name' => 'Saturnq solutions private limited',
            'email' => 'saturnq@gmail.com',
            'mobile' => '9442640132',
            'password' => bcrypt("Date@123"),
            'user_type' => 'C'
        ]);

        DB::table('users')->insert([
            'name' => 'Prasanna',
            'email' => 'prasanna@gmail.com',
            'mobile' => '9442640132',
            'password' => bcrypt("Date@123"),
            'user_type' => 'E',
            'branch_id' => 1,
            'customer_id' => 1
        ]);
    
        DB::table('customers')->insert([
            'customer_name' => 'Saturnq solutions private limited',
            'user_id' => 1,
            'email' => 'saturnq@gmail.com',
            'mobile' => '9442640132',
            'addresss_1' => '#509, 1st floor, red rose plaza',
            'addresss_2' => 'DB road, RS Puram',
            'city_id' => 1,
            'state_id' => 1,
            'country_id' => 1,
            'is_verify_email' => 1,
            'is_verify_mobile' => 1
        ]);

        DB::table('branches')->insert([
            'customer_id' => 1,
            'cin' => 'SQS10',
            'branch_name' => 'SQS - CBE',
            'short_name' => 'SQS',
            'addresss_1' => '#509, 1st floor, red rose plaza',
            'addresss_2' => 'DB road, RS Puram',
            'city_id' => 1,
            'state_id' => 1,
            'country_id' => 1,
            'latitude' => '11.0069882',
            'longitude' => '76.9485465',
            'city_name' => 'Coimbatore',
            'state_name' => 'Tamilnadu',
            'country_name' => 'India',
            'pincode' => '641045',
        ]);

         DB::table('attendance_configs')->insert([
            'branch_id' => 1,
            'customer_id' => 1,
            'is_rh_approve' => 1
        ]);

        DB::table('roles')->insert([
            'role_name' => 'Software Developer'
        ]);
    
        DB::table('employees')->insert([
            'customer_id' => 1,
            'branch_id' => 1,
            'user_id' => 2,
            'first_name' => 'Prasanna',
            'last_name' => 'Raja',
            'emp_email' => 'prasanna@gmail.com',
            'emp_mobile' => '9442640132',
            'emp_code' => 'SQS001',
            'role_id' => 1,
            'reporting_head_id' => 1,
            'is_reporting_head' => 1,
            'shift_id' => 1
        ]);
    
        DB::table('shifts')->insert([
            'shift_name' => 'Day shift',
            'start_time' => '09:00:00',
            'end_time' => '18:30:00'
        ]);
    
        DB::table('branch_previleges')->insert([ [
            'branch_id' => 1,
            'customer_id' => 1,
            'previlege_id' => 1,
            'answer' => 1
        ], [
            'branch_id' => 1,
            'customer_id' => 1,
            'previlege_id' => 2,
            'answer' => 1
        ], [
            'branch_id' => 1,
            'customer_id' => 1,
            'previlege_id' => 3,
            'answer' => 1
        ], [
            'branch_id' => 1,
            'customer_id' => 1,
            'previlege_id' => 4,
            'answer' => 1
        ], [
            'branch_id' => 1,
            'customer_id' => 1,
            'previlege_id' => 5,
            'answer' => 1
        ], [
            'branch_id' => 1,
            'customer_id' => 1,
            'previlege_id' => 6,
            'answer' => 1
        ], [
            'branch_id' => 1,
            'customer_id' => 1,
            'previlege_id' => 7,
            'answer' => 1
        ], [
            'branch_id' => 1,
            'customer_id' => 1,
            'previlege_id' => 8,
            'answer' => 1
        ], [
            'branch_id' => 1,
            'customer_id' => 1,
            'previlege_id' => 9,
            'answer' => 1
        ], [
            'branch_id' => 1,
            'customer_id' => 1,
            'previlege_id' => 10,
            'answer' => 1
        ], [
            'branch_id' => 1,
            'customer_id' => 1,
            'previlege_id' => 11,
            'answer' => 1
        ] ]);
    
        // DB::select( DB::raw("CREATE OR REPLACE FUNCTION calculate_distance(lat1 float, lon1 float, lat2 float, lon2 float, units varchar)
        // RETURNS float AS $dist$
        //     DECLARE
        //         dist float = 0;
        //         radlat1 float;
        //         radlat2 float;
        //         theta float;
        //         radtheta float;
        //     BEGIN
        //         IF lat1 = lat2 OR lon1 = lon2
        //             THEN RETURN dist;
        //         ELSE
        //             radlat1 = pi() * lat1 / 180;
        //             radlat2 = pi() * lat2 / 180;
        //             theta = lon1 - lon2;
        //             radtheta = pi() * theta / 180;
        //             dist = sin(radlat1) * sin(radlat2) + cos(radlat1) * cos(radlat2) * cos(radtheta);
        
        //             IF dist > 1 THEN dist = 1; END IF;
        
        //             dist = acos(dist);
        //             dist = dist * 180 / pi();
        //             dist = dist * 60 * 1.1515;
        
        //             IF units = 'K' THEN dist = dist * 1.609344; END IF;
        //             IF units = 'N' THEN dist = dist * 0.8684; END IF;
        
        //             RETURN dist;
        //         END IF;
        //     END;
        // $dist$ LANGUAGE plpgsql") );
    }


    
    
}

